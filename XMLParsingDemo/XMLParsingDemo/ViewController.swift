//
//  ViewController.swift
//  XMLParsingDemo
//
//  Created by kpugame on 2020/05/19.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController, XMLParserDelegate , UITableViewDataSource {
    
    /* outlets */
    @IBOutlet weak var tableData: UITableView!
    
    /* XML 관련 변수 */
    //  xml file을 download, parse하는 오브젝트
    var parser = XMLParser()
    //  feed 데이터를 저장하는 mutable array
    var post = NSMutableArray()
    //  title, date 같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()
    //  저장 문자열 변수 
    var title1 = NSMutableString()
    var date = NSMutableString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
    }
    
    /* 파싱 함수 */
    func beginParsing() {
        post = []
        parser = XMLParser(contentsOf: (URL(string: "http://images.apple.com/main/rss/hotnews/hotnews.rss"))!)!
        parser.delegate = self
        parser.parse()
        tableData!.reloadData()
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        /* parser가 새로운 element를 발견하면 변수를 생성한다. */
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item") {
            elements = NSMutableDictionary()
            elements = [:]
            title1 = NSMutableString()
            title1 = ""
            date = NSMutableString()
            date = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        /* title & pubDate을 발견하면 title1과 date에 완성한다. */
        if element.isEqual(to: "title") {
            title1.append(string)
        } else if element.isEqual(to: "pubDate") {
            date.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        /* element의 끝에서 feed데이터를 dictionary에 저장 */
        if (elementName as NSString).isEqual(to: "item") {
            if !title1.isEqual(nil) {
                elements.setObject(title1, forKey: "title" as NSCopying)
            }
            if !date.isEqual(nil) {
                elements.setObject(date, forKey: "date" as NSCopying)
            }
            post.add(elements)
        }
    }
    
    /* 테이블 뷰 함수 */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return post.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "title") as! NSString as String
        cell.detailTextLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "date") as! NSString as String
        return cell
    }

}

