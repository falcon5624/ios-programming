//
//  Player.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class Player {
    var cards = [Card]()
    private var N : Int = 0
    private var name : String
    
    init(name : String) {
        self.name = name
    }
    func inHand() -> Int {
        return self.N
    }
    func addCard(c: Card) {
        cards.append(c)
        self.N += 1
    }
    func reset() {
        self.N = 0
        cards.removeAll()
    }
    func value() -> Int {
        var total : Int = 0
        var ace : Int = 0
        
        for card in cards {
            if card.getValue() == 1 {
                ace += 1
            } else {
                total += card.getValue()
            }
        }
        
        if (total + ace * 11) > 21 {
            total += ace * 1
        } else {
            total += ace * 11
        }
        
        return total
    }
}
