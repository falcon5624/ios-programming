//
//  Shuffle.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

extension Array {
    mutating func shuffle() {
        if count < 2 {
            return
        }
        for i in 0..<(count-1) {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            if i != j {
                self.swapAt(i, j)
            }
        }
    }
}
