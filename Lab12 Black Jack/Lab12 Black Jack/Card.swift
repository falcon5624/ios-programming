//
//  Card.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class Card {
    private var value : Int
    private var x : Int
    private var suit : String
    init(temp : Int) {
        self.value = temp % 13 + 1
        self.x = temp / 13
        self.suit = ""
    }
    func getValue() -> Int {
        if self.value > 10 {
            return 10
        } else {
            return self.value
        }
    }
    func getSuit() -> String {
        switch x {
        case 0:
            self.suit = "Clubs"
            break
        case 1:
            self.suit = "Spades"
            break
        case 2:
            self.suit = "Hearts"
            break
        case 3:
            self.suit = "Diamonds"
            break
        default:
            break
        }
        return self.suit
    }
    func filename() -> String {
        return getSuit() + String(self.value)
    }
}
