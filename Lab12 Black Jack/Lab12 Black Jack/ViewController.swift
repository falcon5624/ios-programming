//
//  ViewController.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    /* game variables */
    var player : Player = Player(name : "player")
    var dealer : Player = Player(name : "dealer")
    var audioController : AudioController
    required init?(coder: NSCoder) {
        audioController = AudioController()
        audioController.preloadAudioEffects(audioFileNames: AudioEffectFiles)
        super.init(coder : coder)
    }
    
    var LCardsPlayer = [UIImageView]()
    var LCardsDealer = [UIImageView]()
    
    var deck = [Int]()
    var deckIndex = 0
    
    var nBetMoney : Int = 0
    var nPlayerMoney : Int = 1000
    var nCardsDealer : Int = 0
    var nCardsPlayer : Int = 0
    
    /* outlets */
    @IBOutlet weak var dealerPts: UILabel!
    @IBOutlet weak var playerPts: UILabel!
    @IBOutlet weak var betMoney: UILabel!
    @IBOutlet weak var playerMoney: UILabel!
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var outletBet10: UIButton!
    @IBOutlet weak var outletBet25: UIButton!
    @IBOutlet weak var outletBet50: UIButton!
    @IBOutlet weak var outletHit: UIButton!
    @IBOutlet weak var outletStand: UIButton!
    @IBOutlet weak var outletDeal: UIButton!
    @IBOutlet weak var outletAgain: UIButton!
    
    /* action functions */
    @IBAction func Bet10(_ sender: Any) {
        nBetMoney += 10
        if nBetMoney <= nPlayerMoney {
            betMoney.text = "$\(nBetMoney)"
            betMoney.sizeToFit()
            nPlayerMoney -= 10
            playerMoney.text = "You have $\(nPlayerMoney)"
            playerMoney.sizeToFit()
            
            outletDeal.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney -= 10
        }
    }
    @IBAction func Bet25(_ sender: Any) {
        nBetMoney += 25
        if nBetMoney <= nPlayerMoney {
            betMoney.text = "$\(nBetMoney)"
            betMoney.sizeToFit()
            nPlayerMoney -= 25
            playerMoney.text = "You have $\(nPlayerMoney)"
            playerMoney.sizeToFit()
            
            outletDeal.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney -= 25
        }
    }
    @IBAction func Bet50(_ sender: Any) {
        nBetMoney += 50
        if nBetMoney <= nPlayerMoney {
            betMoney.text = "$\(nBetMoney)"
            betMoney.sizeToFit()
            nPlayerMoney -= 50
            playerMoney.text = "You have $\(nPlayerMoney)"
            playerMoney.sizeToFit()
            
            outletDeal.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney -= 50
        }
    }
    @IBAction func Hit(_ sender: Any) {
        nCardsPlayer += 1
        hitPlayer(n: nCardsPlayer)
        if player.value() > 21 {
            checkWinner()
            outletBet50.isEnabled = false
            outletBet25.isEnabled = false
            outletBet10.isEnabled = false
            outletHit.isEnabled = false
            outletAgain.isEnabled = true
            outletDeal.isEnabled = false
            outletStand.isEnabled = false
        }
    }
    @IBAction func Stand(_ sender: Any) {
        checkWinner()
        
        outletBet50.isEnabled = false
        outletBet25.isEnabled = false
        outletBet10.isEnabled = false
        outletHit.isEnabled = false
        outletAgain.isEnabled = true
        outletDeal.isEnabled = false
        outletStand.isEnabled = false
    }
    @IBAction func Deal(_ sender: Any) {
        deal()
        outletBet50.isEnabled = false
        outletBet25.isEnabled = false
        outletBet10.isEnabled = false
        outletHit.isEnabled = true
        outletAgain.isEnabled = false
        outletDeal.isEnabled = false
        outletStand.isEnabled = true
    }
    @IBAction func Again(_ sender: Any) {
        outletBet50.isEnabled = true
        outletBet25.isEnabled = true
        outletBet10.isEnabled = true
        outletHit.isEnabled = false
        outletAgain.isEnabled = false
        outletDeal.isEnabled = false
        outletStand.isEnabled = false
        player.reset()
        dealer.reset()
        
        for card in LCardsDealer {
            card.removeFromSuperview()
        }
        for card in LCardsPlayer {
            card.removeFromSuperview()
        }
        
        LCardsDealer = [UIImageView]()
        LCardsPlayer = [UIImageView]()
        
        
        dealerPts.text = "Dealer: ?"
        status.text = "Please Bet!"
        playerPts.text = "Player: " + String(player.value())
        
    }
    
    func checkWinner() {
        LCardsDealer[0].removeFromSuperview()
        let newImageView = UIImageView(image: UIImage(named: dealer.cards[0].filename())!)
        newImageView.center = CGPoint(x: 300, y: 250)
        
        self.view.insertSubview(newImageView, belowSubview: LCardsDealer[1])
        LCardsDealer[0] = newImageView
        
        while dealer.value() < 17 {
            nCardsDealer += 1
            hitDealer(n: nCardsDealer)
            
        }
        
        dealerPts.text = "Dealer: " + String(dealer.value())
        
        if player.value() > 21 {
            status.text = "Player Busts"
            audioController.playerEffect(name: SoundWrong)
        } else if dealer.value() > 21 {
            status.text = "Dealer Busts"
            nPlayerMoney += nBetMoney * 2
            audioController.playerEffect(name: SoundWin)
        } else if dealer.value() == player.value() {
            status.text = "Push"
            nPlayerMoney += nBetMoney
            audioController.playerEffect(name: SoundDing)
        } else if dealer.value() < player.value() {
            status.text = "You won!"
            nPlayerMoney += nBetMoney * 2
            audioController.playerEffect(name: SoundWin)
        } else {
            status.text = "Sorry You Lost"
            audioController.playerEffect(name: SoundWrong)
        }
        nBetMoney = 0
        playerMoney.text = "You have $\(nPlayerMoney)"
        playerMoney.sizeToFit()
        betMoney.text = "$" + String(nBetMoney)
    }
    
    func deal() {
        deck.shuffle()
        deckIndex = 0
        
        player.reset()
        dealer.reset()
        
        hitPlayer(n:0)
        hitPlayer(n:1)
        hitDealerDown()
        hitDealer(n:0)
        nCardsDealer = 0
        nCardsPlayer = 1
        
        playerPts.text = "Player : " + String(player.value())
    }
    func hitDealerDown() {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        dealer.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: "b1fv")!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 300, y: 250)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsDealer.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func hitDealer(n: Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        dealer.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 300 + (n+1) * 50, y: 250)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsDealer.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func hitPlayer(n: Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        player.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 300 + n * 50, y: 450)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsPlayer.append(newImageView)
        playerPts.text = "Player: " + String(player.value())
        audioController.playerEffect(name: SoundFlip)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        outletDeal.isEnabled = false
        outletStand.isEnabled = false
        outletAgain.isEnabled = false
        outletHit.isEnabled = false
        for i in 0...51 {
            deck.append(i)
        }
        
        betMoney.text = "$\(nBetMoney)"
        playerMoney.text = "You have $\(nPlayerMoney)"
        dealerPts.text = "Dealer: ?"
        status.text = "Please Bet!"
        playerPts.text = "Player: " + String(player.value())
    }


}

