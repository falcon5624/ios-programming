//
//  totobuttLib.swift
//  Simple Calculator
//
//  Created by kpugame on 2020/03/29.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

infix operator **
func **(lhs : Double, rhs : Double) -> Double {
    return pow(lhs, rhs)
}
func **(lhs: Double, rhs : Int) -> Double {
    return pow(lhs, Double(rhs))
}

