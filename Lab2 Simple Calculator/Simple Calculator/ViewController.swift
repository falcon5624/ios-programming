//
//  ViewController.swift
//  Simple Calculator
//
//  Created by kpugame on 2020/03/28.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var calculator = Calculator()
    var isOperPushed : Bool = false
    var command : String = ""
    var operLeftStr : String = ""
    var operRightStr : String = ""
    var isLeftMinus : Bool = false
    var isRightMinus : Bool = false
    var operLeftSign : String = ""
    var operRightSign : String = ""
    var retVal : String = ""
    
    
    @IBOutlet var boardLabel : UILabel!
    
    @IBAction func Pushed_0() {
        /* 이미 0이 찍혔다면 더 찍는 것은 의미가 없다. */
        if isOperPushed {
            if operRightStr != "0" {
                operRightStr += "0"
            }
        } else {
            if operLeftStr != "0" {
                operLeftStr += "0"
            }
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_1() {
        if isOperPushed {
            operRightStr += "1"
        } else {
            operLeftStr += "1"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_2() {
        if isOperPushed {
            operRightStr += "2"
        } else {
            operLeftStr += "2"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_3() {
        if isOperPushed {
            operRightStr += "3"
        } else {
            operLeftStr += "3"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_4() {
        if isOperPushed {
            operRightStr += "4"
        } else {
            operLeftStr += "4"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_5() {
        if isOperPushed {
            operRightStr += "5"
        } else {
            operLeftStr += "5"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_6() {
        if isOperPushed {
            operRightStr += "6"
        } else {
            operLeftStr += "6"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_7() {
        if isOperPushed {
            operRightStr += "7"
        } else {
            operLeftStr += "7"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_8() {
        if isOperPushed {
            operRightStr += "8"
        } else {
            operLeftStr += "8"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_9() {
        if isOperPushed {
            operRightStr += "9"
        } else {
            operLeftStr += "9"
        }
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_Add() {
        command = "+"
        isOperPushed = true;
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_Subtract() {
        command = "-"
        isOperPushed = true;
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_Multiply() {
        command = "x"
        isOperPushed = true;
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_Divide() {
        command = "/"
        isOperPushed = true;
        ModifyLabelText(inOption: .half)
    }
    @IBAction func Pushed_Return() {
        
        /* 점 인덱스 구하기 위한 방법 */
        let leftPointIdx : String.Index = operLeftStr.firstIndex(of: ".") ?? operLeftStr.endIndex
        let rightPointIdx : String.Index = operRightStr.firstIndex(of: ".") ?? operRightStr.endIndex
        
        /* 소수부 시작 인덱스*/
        let lPNextIdx : String.Index
        let rPNextIdx : String.Index
        
        /* 소숫점 입력 유무 예외처리*/
        if operLeftStr.firstIndex(of: ".") == nil {
            lPNextIdx = operLeftStr.endIndex
        } else {
            lPNextIdx = operLeftStr.index(after: leftPointIdx)
        }
        if operRightStr.firstIndex(of: ".") == nil {
            rPNextIdx = operRightStr.endIndex
        } else {
            rPNextIdx = operRightStr.index(after: rightPointIdx)
        }
         
        /* 정수부 문자열 */
        let operLeftIntStr = operLeftStr[operLeftStr.startIndex ..< leftPointIdx]
        let operRightIntStr = operRightStr[operRightStr.startIndex ..< rightPointIdx]
        /* 정수부 */
        let operLeftIntNum : Int = Int(operLeftIntStr) ?? 0
        let operRightIntNum : Int = Int(operRightIntStr) ?? 0
        
        /* 소수부 문자열*/
        let operLeftDeci = operLeftStr[lPNextIdx...]
        let operRightDeci = operRightStr[rPNextIdx...]
        /* 소수부 나누기 위한 10의 지수승 */
        let leftExp : Double = pow(10.0, Double(operLeftDeci.count))
        let rightExp : Double = pow(10.0, Double(operRightDeci.count))
        /* 소수부 */
        let operLeftDeciNum : Int = Int(operLeftDeci) ?? 0
        let operRightDeciNum : Int = Int(operRightDeci) ?? 0
        
        /* 실제 숫자 */
        var operLeftnum = Double(operLeftIntNum) + Double(operLeftDeciNum) / leftExp
        var operRightnum = Double(operRightIntNum) + Double(operRightDeciNum) / rightExp
        
        /* 부호 변환 */
        if (isLeftMinus) {
            operLeftnum = -1.0 * operLeftnum
        }
        if (isRightMinus) {
            operRightnum = -1.0 * operRightnum
        }
        
        /* 디버그용 출력 */
        print("\(operLeftnum) \(command) \(operRightnum)")
        
        /* 연산자에 따른 함수 수행 */
        switch command {
        case "":
            break;
        case "-":
            retVal = calculator.Subtract(inLeft: operLeftnum, inRight: operRightnum)
            break;
        case "+":
            retVal = calculator.Add(inLeft: operLeftnum, inRight: operRightnum)
            break;
        case "x":
            retVal = calculator.Multiply(inLeft: operLeftnum, inRight: operRightnum)
            break;
        case "/":
            retVal = calculator.Divide(inLeft: operLeftnum, inRight: operRightnum)
            break;
        default:
            break;
        }
        isOperPushed = false
        ModifyLabelOptions()
        ModifyLabelText(inOption: .full)
        print(retVal)
    }
    @IBAction func Pushed_Clear() {
        isOperPushed = false
        isLeftMinus = false
        isRightMinus = false
        command = ""
        operLeftStr = ""
        operRightStr = ""
        operLeftSign = ""
        operRightSign = ""
        boardLabel.text = "\(operLeftSign)\(operLeftStr) \(command) \(operRightSign)\(operRightStr)"
    }
    @IBAction func Pushed_Point() {
        /* 소숫점 입력 버튼 */
        if isOperPushed {
            operRightStr += "."
        } else {
            operLeftStr += "."
        }
        boardLabel.text = "\(operLeftSign)\(operLeftStr) \(command) \(operRightSign)\(operRightStr)"
    }
    @IBAction func Pushed_SignChange() {
        /* 부호 변환 버튼 */
        if !isOperPushed {
            isLeftMinus = !isLeftMinus
            if isLeftMinus {
                operLeftSign = "-"
            } else {
                operLeftSign = ""
            }
        } else {
            isRightMinus = !isRightMinus
            if isRightMinus {
                operRightSign = "-"
            } else {
                operRightSign = ""
            }
        }
        ModifyLabelText(inOption: .half)
    }
    
    /* 기능 함수 */
    func ModifyLabelOptions() {
        /* 라벨 객체의 멤버들을 바꿉니다.
         현재 100% 유효하게 작동되지 않습니다. 더 공부해봐야 합니다. */
        boardLabel.numberOfLines = 0    //  no limit
        boardLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        boardLabel.minimumScaleFactor = 10.0
        boardLabel.adjustsFontSizeToFitWidth = true
    }
    
    func ModifyLabelText(inOption : TextOption) {
        /* 라벨의 출력식을 바꿉니다. */
        switch inOption {
        case .full:
            boardLabel.text = "\(operLeftSign)\(operLeftStr) \(command) \(operRightSign)\(operRightStr)\n= \(retVal)"
            break;
        case .half:
            boardLabel.text = "\(operLeftSign)\(operLeftStr) \(command) \(operRightSign)\(operRightStr)"
            break;
        default:
            break;
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ModifyLabelOptions()
        boardLabel.text = "\(operLeftSign)\(operLeftStr) \(command) \(operRightSign)\(operRightStr)"
    }
    
}

