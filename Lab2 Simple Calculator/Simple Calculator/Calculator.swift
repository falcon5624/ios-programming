//
//  Calculator.swift
//  Simple Calculator
//
//  Created by kpugame on 2020/03/28.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class Calculator {
    private var operLeft : Double = 0.0     //  좌항 피연산자
    private var operRight : Double = 0.0    //  우항 피연산자
    private var retValue : Double = 0.0     //  결과값
    
    init () {
        print("## Calculator is Initiated ##")
        self.Clear()
    }
    
    private func PutIn(inLeft : Double, inRight : Double) {
        self.operLeft = inLeft
        self.operRight = inRight
    }
    /* 계산기 연산자 수행 함수 */
    func Add(inLeft : Double, inRight : Double) -> String{
        self.PutIn(inLeft: inLeft, inRight: inRight)
        self.retValue = self.operLeft + self.operRight
        return String(self.retValue)
    }
    
    func Subtract(inLeft : Double, inRight : Double) -> String{
        self.PutIn(inLeft: inLeft, inRight: inRight)
        self.retValue = self.operLeft - self.operRight
        return String(self.retValue)
    }

    func Divide(inLeft : Double, inRight : Double) -> String{
        self.PutIn(inLeft: inLeft, inRight: inRight)
        self.retValue = self.operLeft / self.operRight
        return String(self.retValue)
    }
    
    func Multiply(inLeft : Double, inRight : Double) -> String {
        self.PutIn(inLeft: inLeft, inRight: inRight)
        self.retValue = self.operLeft * self.operRight
        return String(self.retValue)
    }
    
    private func Clear() {
        self.operLeft = 0.0
        self.operRight = 0.0
        self.retValue = 0.0
    }
}

public enum TextOption : Int {
    /* 라벨에 계산기 수식을 어떻게 보여줄 것인가? */
    case full = 0   //  operandA operator operandB\n= returnValue
    case half = 1   //  operandA operator operandB
}
