//
//  Ranker.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/12.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class CRanker {
    var summonerName : String
    var leaguePoints : Int
    var rank : String
    var wins : Int
    var losses : Int
    
    init() {
        self.summonerName = ""
        self.leaguePoints = 0
        self.rank = ""
        self.wins = 0
        self.losses = 0
    }
    
    func setProperties(summonerName : String, leaguePoints : Int, rank : String, wins : Int, losses : Int) {
        self.summonerName = summonerName
        self.leaguePoints = leaguePoints
        self.rank = rank
        self.wins = wins
        self.losses = losses
    }
}
