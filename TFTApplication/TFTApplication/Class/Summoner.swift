//
//  Summoner.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/06.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

internal class CSummoner {
    /* 소환사 정보 클래스 */
    var m_id : String             // Encrypted summoner ID. Max length 63 characters.
    var m_accountId : String      // Encrypted account ID. Max length 56 characters.
    var m_puuid : String          // Encrypted PUUID. Exact length of 78 characters.
    var m_name : String           // Summoner name.
    var m_profileIconId : Int     // ID of the summoner icon associated with the summoner.
    var m_summonerLevel : Int     // Summoner level associated with the summoner.
    var m_leagueEntryDTO : LeagueEntryDTO
    
    init() {
        m_id = ""
        m_accountId = ""
        m_puuid = ""
        m_name = ""
        m_profileIconId = 0
        m_summonerLevel = 0
        self.m_leagueEntryDTO = LeagueEntryDTO()
    }
    
    init (id : String, accountId : String, puuid : String, name : String, profileIconId : Int, summonerLevel : Int, leagueEntryDTO : LeagueEntryDTO) {
        m_id = id
        m_accountId = accountId
        m_puuid = puuid
        m_name = name
        m_profileIconId = profileIconId
        m_summonerLevel = summonerLevel
        self.m_leagueEntryDTO = leagueEntryDTO
    }
    
    func setLeagueEntryDTO(inputData : LeagueEntryDTO) {
        self.m_leagueEntryDTO = inputData
    }
}
