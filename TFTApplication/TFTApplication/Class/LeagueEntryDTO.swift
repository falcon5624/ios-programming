//
//  LeagueEntryDTO.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/11.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

struct LeagueEntryDTO {
    var leagueId : String
    var queueType : String
    var tier : String
    var rank : String
    var leaguePoints : Int
    var wins : Int              // First placement.
    var losses : Int            // Second through eighth placement.
    var hotStreak : Bool        // 연승 상태 (로 유추하고 있음)
    var veteran : Bool          // 정보 없음..
    var freshBlood : Bool       // 배치고사 상태 (로 유추하고 있음)
    
    init() {
        self.leagueId = ""
        self.queueType = ""
        self.tier = ""
        self.rank = ""
        self.leaguePoints = 0
        self.wins = 0
        self.losses = 0
        self.hotStreak = false
        self.veteran = false
        self.freshBlood = false
    }
}
