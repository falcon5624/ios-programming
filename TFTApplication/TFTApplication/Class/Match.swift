//
//  Match.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/12.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

/* DTO, Data Transfer Object */

struct UnitDto {
    var items : [Int]           // A list of the unit's items. Please refer to the Teamfight Tactics documentation for item ids.
    var character_id : String   // This field was introduced in patch 9.22 with data_version 2.
    var name : String           // Unit name.
    var rarity : Int            // Unit rarity. This doesn't equate to the unit cost.
    var tier : Int              // Unit tier.
}

struct TraitDto {
    var name : String           // Trait name.
    var num_units : Int         // Number of units with this trait.
    var tier_current : Int      // Current active tier for the trait.
    var tier_total : Int        // Total tiers for the trait.
}

struct ParticipantDto {
    var gold_left : Int                 // Gold left after participant was eliminated.
    var last_round : Int                // The round the participant was eliminated in. Note: If the player was eliminated in stage 2-1 their last_round would be 5.
    var level : Int                     // Participant Little Legend level. Note: This is not the number of active units.
    var placement : Int                 // Participant placement upon elimination.
    var players_eliminated : Int        // Number of players the participant eliminated.
    var time_eliminated : Float         // The number of seconds before the participant was eliminated.
    var total_damage_to_players : Int   // Damage the participant dealt to other players.
    var traits : TraitDto               // A complete list of traits for the participant's active units.
    var units : UnitDto                 // A list of active units for the participant.
}

struct InfoDto {
    var game_datetime : Int             // Unix timestamp.
    var game_length : Float             // Game length in seconds.
    var game_variation : String         // Game variation key. Game variations documented in TFT static data.
    var game_version : String           // Game client version.
    var participants : [ParticipantDto] // Participants
    var queue_id : Int                  // Please refer to the League of Legends documentation.
    var tft_set_number : Int            // Teamfight Tactics set number.
}

struct MetadataDto {
    var data_version : String = ""      // Match data version.
    var match_id : String = ""          // Match id.
    var participants : [String] = []    // A list of encrypted participant PUUIDs.
}
    
struct MatchDto {
    var metadata : MetadataDto          // Match metadata.
    var info : InfoDto                  // Match Info.
}

