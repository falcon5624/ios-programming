//
//  SummonerInfoViewController.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/06.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit
import SwiftUI



class SummonerInfoViewController: UIViewController {

    /* member variables */
    var m_summoner : CSummoner!
    
    /* outlets */
    @IBOutlet weak var winrateImageView: UIImageView!
    @IBOutlet weak var profileIconImageView: UIImageView!
    @IBOutlet weak var tierIconImageView: UIImageView!
    @IBOutlet weak var summonerNameLabel: UILabel!
    @IBOutlet weak var tierLabel: UILabel!
    @IBOutlet weak var winsLabel: UILabel!
    @IBOutlet weak var lossesLabel: UILabel!
    @IBOutlet weak var hotStreakLabel: UILabel!
    @IBOutlet weak var winRateLabel: UILabel!
    @IBOutlet weak var leaguePointLabel: UILabel!
    @IBOutlet weak var summonerLevelLabel: UILabel!
    var timer = Timer()                     //  애니메이션을 위한 타이머 객체
    
    func drawRect() {
        UIGraphicsBeginImageContext(winrateImageView.frame.size)
        let context = UIGraphicsGetCurrentContext()!
        
        // Draw Rectangle
        context.setLineWidth(2.0)
        context.setFillColor(UIColor.red.cgColor)
        
        // (50,100) 위치에서 가로 200, 세로 200 크기의 사각형 추가
        //context.addRect()
        context.fill(CGRect(x: 0, y: 0, width: 100, height: 200))
        context.strokePath()
        
        context.setLineWidth(2.0)
        context.setFillColor(UIColor.blue.cgColor)
        
        let height = (Double(self.m_summoner.m_leagueEntryDTO.wins) * 200.0) / Double(self.m_summoner.m_leagueEntryDTO.wins + self.m_summoner.m_leagueEntryDTO.losses)
        print(height)
        context.fill(CGRect(x: 0, y: 200 - height, width: 100, height: height))
        
        winrateImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }
    
    /* custom functions */
    @objc func update() {
        /* Finish Game */
        let startX : CGFloat = CGFloat(arc4random_uniform(UINT32_MAX) % 414)
        let startY : CGFloat = CGFloat(arc4random_uniform(UINT32_MAX) % 896)
        
        let stars = StardustView(frame: CGRect(x: startX, y: startY, width: CGFloat(arc4random_uniform(UINT32_MAX) % 30), height: CGFloat(arc4random_uniform(UINT32_MAX) % 30)))
        self.view.addSubview(stars)
        self.view.sendSubviewToBack(_: stars)
        
        UIView.animate(withDuration: 3.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
            stars.center = CGPoint(x: startX, y: startY)
        }, completion: {
            (value:Bool) in stars.removeFromSuperview()
        })
    }
    func SetTimer() {
        /* 타이머 설정 함수 */
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(1.0), target: self, selector: #selector(self.update), userInfo: nil, repeats: true)
    }
    func setSummonerView() {
        // set textLabels
        self.summonerNameLabel.text = "소환사명: " + self.m_summoner.m_name
        self.summonerLevelLabel.text = "레벨: \(self.m_summoner.m_summonerLevel)" 
        self.tierLabel.text = self.m_summoner.m_leagueEntryDTO.tier + " " + self.m_summoner.m_leagueEntryDTO.rank
        var textColor : UIColor
        switch self.m_summoner.m_leagueEntryDTO.tier {
        case "IRON":
            textColor = UIColor(red: 0.41, green: 0.41, blue: 0.41, alpha: 1.0)
            break
        case "BRONZE":
            textColor = UIColor(red: 0.8, green: 0.5, blue: 0.2, alpha: 1.0)
            break
        case "SILVER":
            textColor = UIColor(red: 0.65, green: 0.65, blue: 0.65, alpha: 1.0)
            break
        case "GOLD":
            textColor = UIColor(red: 0.83, green: 0.69, blue: 0.22, alpha: 1.0)
            break
        case "PLATINUM":
            textColor = UIColor(red: 0.0, green: 0.42, blue: 0.24, alpha: 1.0)
            break
        case "DIAMOND":
            textColor = UIColor(red: 0.95, green: 0.61, blue: 0.73, alpha: 1.0)
            break
        case "MASTER":
            textColor = UIColor(red: 0.59, green: 0.44, blue: 0.84, alpha: 1.0)
            break
        case "GRANDMASTER":
            textColor = UIColor(red: 0.89, green: 0.15, blue: 0.21, alpha: 1.0)
            break
        case "CHALLENGER":
            textColor = UIColor(red: 0.73, green: 0.95, blue: 1, alpha: 1.0)
            break
        default:
            fatalError("Tier Error : Out of Range")
        }
        self.tierLabel.textColor = textColor
        self.winsLabel.text = "승리: " + String(self.m_summoner.m_leagueEntryDTO.wins)
        self.lossesLabel.text = "패배: " + String(self.m_summoner.m_leagueEntryDTO.losses)
        self.winRateLabel.text = String(format: "승률: %.1f%%",
                                        (Double(self.m_summoner.m_leagueEntryDTO.wins) * 100.0) / Double(self.m_summoner.m_leagueEntryDTO.wins + self.m_summoner.m_leagueEntryDTO.losses))
        self.hotStreakLabel.text = "연승 상태: \(self.m_summoner.m_leagueEntryDTO.hotStreak)"
        self.leaguePointLabel.text = "리그 포인트: \(self.m_summoner.m_leagueEntryDTO.leaguePoints)"
        
        // set imageViews
        // set profileIcon Image from url to imageView
        let profileIconURL = "https://ddragon.leagueoflegends.com/cdn/10.11.1/img/profileicon/\(self.m_summoner.m_profileIconId).png"
        
        if let imageInstance = URL(string: profileIconURL) {
            let data = try? Data(contentsOf: imageInstance)
            let image = UIImage(data: data!)
            profileIconImageView.image = image
        }
        tierIconImageView.image = UIImage(named: "Emblem_" + self.m_summoner.m_leagueEntryDTO.tier)
        
    }
    
    /* action functions */
    @IBAction func PushBack(_ sender: Any) {
        performSegue(withIdentifier: "backFromSummoner", sender: self)
    }
    
    /* override functions */
    override func viewDidLoad() {
        super.viewDidLoad()

        setSummonerView()
        
        
        
        /* ################################## For Debug */
        print("Scene 2 : Summoner Information Scene")
        
        print(self.m_summoner.m_leagueEntryDTO.leagueId)
        SetTimer()
        
        drawRect()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
