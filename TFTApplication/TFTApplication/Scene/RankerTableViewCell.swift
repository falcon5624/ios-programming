//
//  RankerTableViewCell.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/12.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class RankerTableViewCell: UITableViewCell {
    
    /* outlets */
    @IBOutlet weak var m_summonerNameLabel: UILabel!
    @IBOutlet weak var m_leaguePointsLabel: UILabel!
    @IBOutlet weak var m_tierLabel: UILabel!
    @IBOutlet weak var m_winRateLabel: UILabel!
    @IBOutlet weak var profileIconImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
