//
//  StardustView.swift
//  Anagrams
//
//  Created by KIMYOUNG SIK on 2018. 2. 17..
//  Copyright © 2018년 Caroline. All rights reserved.
//
import UIKit
//ExplodeView와 매우 비슷하다.
class StardustView: UIView {
    private var emitter: CAEmitterLayer!
    
    override class var layerClass: AnyClass {
        return CAEmitterLayer.self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("use init(frame: ")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        emitter = self.layer as! CAEmitterLayer
        emitter.emitterPosition = CGPoint(x: self.bounds.size.width / 2, y: self.bounds.size.height / 2)
        emitter.emitterSize = self.bounds.size
        emitter.renderMode = CAEmitterLayerRenderMode.additive
        emitter.emitterShape = CAEmitterLayerEmitterShape.rectangle
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        if self.superview == nil {
            return
        }
        
        let texture: UIImage? = UIImage(named: "particle")
        assert(texture != nil, "particle image not found")
        
        let emitterCell = CAEmitterCell()
        
        emitterCell.name = "cell"
        emitterCell.contents = texture?.cgImage
        emitterCell.birthRate = 500
        emitterCell.lifetime = 0.5          //ExplodeView보다 lifetime이 길다.
        emitterCell.redRange = Float(drand48())
        emitterCell.redSpeed = Float(drand48())
        emitterCell.blueRange = Float(drand48())
        emitterCell.greenRange = Float(drand48())
        emitterCell.xAcceleration = 0    //오른쪽으로 이동할 때 파티클이 왼쪽으로 흩날린다.
        emitterCell.yAcceleration = 1960     //파티클이 아래로 떨어진다. 중력효과
        emitterCell.velocity = 500
        emitterCell.velocityRange = 500
        emitterCell.scaleRange = 1
        emitterCell.scaleSpeed = -0.3
        emitterCell.emissionRange = CGFloat(Double.pi * 2)
        emitter.emitterCells = [emitterCell]
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
            self.removeFromSuperview()
        })
    }
}

