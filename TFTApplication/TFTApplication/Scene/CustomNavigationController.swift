//
//  CustomNavigationController.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/06.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class CustomNavigationController: UINavigationController {

    /* memo
     한 버튼에서 여러 세그를 주는 것이 불가능하다고 판단되어, 뷰들을 스택으로 관리하는 Navigation Controller에서
     씬 전환 정보를 입력받아 해당 뷰로 이동하도록 처리합니다.
     
     이동 할 뷰 인덱스, 검색할 소환사 기초정보 구조체를 전달받습니다.
     */
    
    /* member variables */
    let m_apiKey : String = g_apiKey
    var m_nViewIndex : Int = 0
    var m_summoner : CSummoner!
    var m_matchIds : [String] = []
    var m_defaultURL : String = ""
    var m_fullURL : String = ""
    
    /* custom functions */
    func resetViewProperties() {
        m_nViewIndex = 0
        m_defaultURL = ""
        m_fullURL = ""
    }
    func debugPrint() {
        print("검색된 소환사명 : \(self.m_summoner.m_name)")
    }
    func makeURL(viewIndex : Int) {
        switch self.m_nViewIndex {
        case 1:
            /* TFT-LEAGUE-V1 API URL */
            self.m_defaultURL = "https://kr.api.riotgames.com/tft/league/v1/entries/by-summoner/"
            self.m_fullURL = self.m_defaultURL + self.m_summoner.m_id + "?api_key=" + self.m_apiKey
            break
        case 2:
            let gameCounts = 20
            self.m_defaultURL = "https://asia.api.riotgames.com/tft/match/v1/matches/by-puuid/"
            self.m_fullURL = self.m_defaultURL + self.m_summoner.m_puuid + "/ids?count=\(gameCounts)" + "&api_key=" + self.m_apiKey
            break
        case 3:
            self.m_defaultURL = "https://kr.api.riotgames.com/tft/league/v1/master?"
            self.m_fullURL = self.m_defaultURL + "api_key=" + self.m_apiKey
            break
            
        default:
            fatalError("Undefined View Number input")
            break
        }
    }
    func setLocalLeagueEntryDTO(data : [String : AnyObject]) -> LeagueEntryDTO {
        var leagueEntryDTO = LeagueEntryDTO()
        leagueEntryDTO.freshBlood = data["freshBlood"] as! Bool
        leagueEntryDTO.hotStreak = data["hotStreak"] as! Bool
        leagueEntryDTO.leagueId = data["leagueId"] as! String
        leagueEntryDTO.leaguePoints = data["leaguePoints"] as! Int
        leagueEntryDTO.losses = data["losses"] as! Int
        leagueEntryDTO.wins = data["wins"] as! Int
        leagueEntryDTO.veteran = data["veteran"] as! Bool
        leagueEntryDTO.tier = data["tier"] as! String
        leagueEntryDTO.rank = data["rank"] as! String
        leagueEntryDTO.queueType = data["queueType"] as! String
        
        return leagueEntryDTO
    }
    
    /* override functions */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* ################################## For Debug */
        print("Scene 1.5 : Navigation State")
    }
    override func viewDidAppear(_ animated: Bool) {
        debugPrint()
        
        switch m_nViewIndex {
        case 0:
            performSegue(withIdentifier: "BackFromCustomNavi", sender: self)
            resetViewProperties()
            break
        case 1:
            self.makeURL(viewIndex: self.m_nViewIndex)
            performSegue(withIdentifier: "ToSummonerInfo", sender: self)
            resetViewProperties()
            break
        case 2:
            self.makeURL(viewIndex: self.m_nViewIndex)
            performSegue(withIdentifier: "ToMatches", sender: self)
            resetViewProperties()
            break
        case 3:
            self.makeURL(viewIndex: self.m_nViewIndex)
            performSegue(withIdentifier: "ToChallengerList", sender: self)
            resetViewProperties()
            break
        default:
            fatalError("Undefined View Number input")
            break
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "BackFromCustomNavi":
            break
            
        case "ToSummonerInfo":
            let summonerInfoViewController = segue.destination as! SummonerInfoViewController
            guard let urlInstance = URL(string: self.m_fullURL) else { return }
                  let optionalData = try? Data(contentsOf: urlInstance)
                  guard let data = optionalData,
                      let json = try? JSONSerialization.jsonObject(with: data) as! [[String : AnyObject]]
                      else { return }
            print(json)
            let leagueEntryDTO = self.setLocalLeagueEntryDTO(data: json[0])
            
            self.m_summoner.setLeagueEntryDTO(inputData: leagueEntryDTO)
            summonerInfoViewController.m_summoner = self.m_summoner
            break
            
        case "ToMatches":
            let matchesViewController = segue.destination as! MatchesViewController
            guard let urlInstance = URL(string: self.m_fullURL) else { return }
                  let optionalData = try? Data(contentsOf: urlInstance)
                  guard let data = optionalData,
                      let json = try? JSONSerialization.jsonObject(with: data) as! [String]
                      else { return }
            
            matchesViewController.m_summoner = self.m_summoner
            matchesViewController.m_matchIds = self.m_matchIds
            break
            
        case "ToChallengerList":
            let challengerListViewController = segue.destination as! ChallengerListViewController
            guard let urlInstance = URL(string: self.m_fullURL) else { return }
            let optionalData = try? Data(contentsOf: urlInstance)
            guard let data = optionalData,
                let json = try? JSONSerialization.jsonObject(with: data) as! [String:AnyObject]
                else { return }
            challengerListViewController.m_json = json
            break
            
        default:
            fatalError("Undefined Segue Identifier Input")
            break
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
