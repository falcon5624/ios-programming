//
//  IntroViewController.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/06.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit
import Speech

class IntroViewController: UIViewController, UIScrollViewDelegate {

    /* memo
     이곳에서 소환사명을 검색하고 분기할 TFT 기능씬을 스크롤 뷰로 선택합니다.
     소환사명은 Real-time Speech Recognition(Only ko-KR)으로 검색될 수 있습니다.
     */
    
    
    /* member variables */
    let m_apiKey : String = g_apiKey
    var m_summonerName_utf8 : String = ""
    let m_defaultURL : String = "https://kr.api.riotgames.com/tft/summoner/v1/summoners/by-name/"
    var m_fullURL : String = ""
    var m_summoner : CSummoner! = CSummoner()
    var m_nViewIndex : Int = 1
    var m_scrollImages : [UIImage] = []
    var m_scrollImageViews : [UIImageView?] = []
        /* 음성인식 관련 변수 */
    var m_bIsListening : Bool = false
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ko-KR"))!
    private var speechRecognitionRequest : SFSpeechAudioBufferRecognitionRequest?
    private var speechRecognitionTask : SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    /* outlets */
    @IBOutlet weak var summonerNameTextField: UITextField!
    @IBOutlet weak var uiSummonerNameLabel: UILabel!
    @IBOutlet weak var uiSpeechRecognizeButton: UIButton!
    @IBOutlet weak var uiScrollView: UIScrollView!
    @IBOutlet weak var uiPageControl: UIPageControl!
    
    /* custom functions */
        /* 뷰, 시스템 로직 관련 함수 */
    func resetViewProperties() {
        self.m_nViewIndex = 0
    }
    func setSummonerData(data : [String : AnyObject]) {
        self.m_summoner.m_accountId = data["accountId"] as! String
        self.m_summoner.m_id = data["id"] as! String
        self.m_summoner.m_puuid = data["puuid"] as! String
        self.m_summoner.m_profileIconId = data["profileIconId"] as! Int
        self.m_summoner.m_summonerLevel = data["summonerLevel"] as! Int
    }
    func loadScrollImages() {
        /* TEST, Version 1 ##################################### */
        for number in 1...3 {
            if let image : UIImage = UIImage(named: "scrollScene\(number)") {
                self.m_scrollImages.append(image)
            }
        }
        
        
        
    }
        /* 음성인식 관련 함수 */
    func authorizeSR() {
        SFSpeechRecognizer.requestAuthorization {
            authStatus in OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.uiSpeechRecognizeButton.isEnabled = true
                    break
                case .denied:
                    self.uiSpeechRecognizeButton.isEnabled = false
                    self.uiSpeechRecognizeButton.setTitle("Speech recognition access denied by user", for: .disabled)
                    break
                case .restricted:
                    self.uiSpeechRecognizeButton.isEnabled = false
                    self.uiSpeechRecognizeButton.setTitle("Speech recognition restricted on device", for: .disabled)
                    break
                case .notDetermined:
                    self.uiSpeechRecognizeButton.isEnabled = false
                    self.uiSpeechRecognizeButton.setTitle("Speech recognition not authorized", for: .disabled)
                    break
                }
            }
        }
    }
    func startSession() throws {
        if let recognitionTask = speechRecognitionTask {
            recognitionTask.cancel()
            self.speechRecognitionTask = nil
        }
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSession.Category.record)
        
        speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let recognitionRequest = speechRecognitionRequest else {
            fatalError("SFSpeechAudioBufferRecognitionRequest object creation failed")
        }
        
        let inputNode = audioEngine.inputNode
        recognitionRequest.shouldReportPartialResults = true
        speechRecognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) {
            result, error in
            var finished = false
            if let result = result {
                self.summonerNameTextField.text = result.bestTranscription.formattedString
                finished = result.isFinal
            }
            if error != nil || finished {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.speechRecognitionRequest = nil
                self.speechRecognitionTask = nil
                self.uiSpeechRecognizeButton.isEnabled = true
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) {
            (buffer : AVAudioPCMBuffer, when : AVAudioTime) in
            self.speechRecognitionRequest?.append(buffer)
        }
        audioEngine.prepare()
        try audioEngine.start()
    }
        /* scrollView 관련 함수 */
    func LoadPage(_ page : Int) {
        if page < 0 || page >= m_scrollImages.count {
            //  페이지 인덱스 범위를 벗어나면 리턴
            return
        }
        
        //  초기에 pageViews 배열은 nil로 이루어져 있는데, nil이 아니라면 로딩이 된 것이므로 아무런 할 일이 없다.
        if m_scrollImageViews[page]  != nil {
            
        } else {
            //  pageView가 nil이면 로딩해야 한다.
            //  y offset은 0, x offset은 화면너비 x 페이지 인덱스로 설정
            var frame = uiScrollView.bounds
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0.0
            //  **************** 다른 부분 ********************
            //  페이지들이 서로 닿지 않도록 수평으로 집어넣은 그림
            frame = frame.insetBy(dx: 10.0, dy: 0.0)
            
            //  새로운 UIImageView를 생성하고 scrollView에 추가한다.
            let newPageView = UIImageView(image: m_scrollImages[page])
            newPageView.contentMode = .scaleAspectFit
            newPageView.frame = frame
            uiScrollView.addSubview(newPageView)
            
            //  새로 생성한 newPageView를 배열의 nil 대신에 삽입한다.
            m_scrollImageViews[page] = newPageView
        }
    }
    func PurgePage(_ page : Int) {
        /* 현재 페이지 앞뒤를 제외하고 나머지는 pageViews 배열에서 제거한다. */
        if page < 0 || page >= m_scrollImages.count {
            //  페이지 인덱스를 벗어나면 리턴
            return
        }
        
        //  pageViews[page]가 nil이 아니라면 scrollView에서 제거하고
        //  pageViews[page]는 nil로 만듦
        if let pageView = m_scrollImageViews[page] {
            pageView.removeFromSuperview()
            m_scrollImageViews[page] = nil
        }
    }
    func LoadVisiblePages() {
        //  count current page number
        let pageWidth = uiScrollView.frame.width
        let numerator = (uiScrollView.contentOffset.x * 2.0 + pageWidth)
        let denominator = (pageWidth * 2.0)
        let page = Int(floor(numerator / denominator))
        
        //  pageControl 현재 페이지 갱신
        uiPageControl.currentPage = page
        
        //  현재 페이지 앞뒤 페이지 설정
        let firstPage = page - 1
        let lastPage = page + 1
        
        //  페이지 0..<앞페이지 전까지 제거
        for index in 0..<firstPage + 1 {
            PurgePage(index)
        }
        
        //  앞, 현재, 뒤 3개 페이지만 로딩
        for index in firstPage ... lastPage {
            LoadPage(index)
        }
        
        //  뒤페이지<..끝페이지까지 제거
        for index in lastPage + 1 ..< m_scrollImages.count + 1 {
            PurgePage(index)
        }
    }
    
    /* action functions */
    @IBAction func PushButton(_ sender: Any) {
        print("Action Event : Button Pushed")
        
        self.m_summoner.m_name = self.summonerNameTextField.text!
        self.m_summonerName_utf8 = self.m_summoner.m_name.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        self.m_fullURL = self.m_defaultURL + self.m_summonerName_utf8 + "?api_key=" + self.m_apiKey
        guard let urlInstance = URL(string: self.m_fullURL) else { return }
        let optionalData = try? Data(contentsOf: urlInstance)
        guard let data = optionalData,
            let json = try? JSONSerialization.jsonObject(with: data) as? [String : AnyObject]
            else { return }
        
        print(json)
        
        setSummonerData(data: json)
    }
    @IBAction func unwind(segue : UIStoryboardSegue) {
        /* IntroView로 돌아오는 unwind 세그웨이 */
        resetViewProperties()
    }
    @IBAction func recognizeButton(_ sender: Any) {
        /* 음성인식 버튼 */
        if m_bIsListening {
            /* 음성인식 멈출 때 */
            uiSpeechRecognizeButton.setTitle("음성인식 시작", for: .normal)
            uiSpeechRecognizeButton.setTitleColor(UIColor.systemBlue, for: .normal)
            if audioEngine.isRunning {
                audioEngine.stop()
                speechRecognitionRequest?.endAudio()
            }
            
            m_bIsListening = false
        }
        else {
            /* 음성인식 시작할 때 */
            uiSpeechRecognizeButton.setTitle("음성인식 중지", for: .normal)
            uiSpeechRecognizeButton.setTitleColor(UIColor.systemRed, for: .normal)
            try! startSession()
            m_bIsListening = true
        }
    }
    
    /* override functions */
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authorizeSR()
        loadScrollImages()
        
        let pageCount = m_scrollImages.count
        uiPageControl.currentPage = 0
        uiPageControl.numberOfPages = pageCount
        for _ in 0..<pageCount {
            m_scrollImageViews.append(nil)
        }
        
        let pageScrollViewSize = uiScrollView.frame.size
        uiScrollView.contentSize = CGSize(width: pageScrollViewSize.width * CGFloat(pageCount), height: pageScrollViewSize.height)
        
        LoadVisiblePages()
        
        /* ################################## For Debug */
        print("Scene 1 : Intro Scroll Scene")
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "ToCustomNavigation":
            let customNavigationController = segue.destination as! CustomNavigationController
            customNavigationController.m_nViewIndex = m_nViewIndex
            customNavigationController.m_summoner = m_summoner
        default:
            fatalError("Undefined Segue Identifier Input")
            break
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //  Load the pages that are now on screen
        LoadVisiblePages()
        m_nViewIndex = uiPageControl.currentPage + 1
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
