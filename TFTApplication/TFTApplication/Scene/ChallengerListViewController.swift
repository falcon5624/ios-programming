//
//  ChallengerListViewController.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/06.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ChallengerListViewController: UIViewController, UITableViewDataSource {

    /* member variables */
    var m_json : [String:AnyObject] = [:]           // 마스터 티어 API 검색 데이터
    var m_tier : String = ""                        // 티어 종류
    var m_name : String = ""                        // 티어 이름
    var m_entries : [[String:AnyObject]] = [[:]]    // 유저 정보 Json 형태
    var m_rankers : [CRanker] = []                  // 유저 배열
    
    /* outlets */
    @IBOutlet weak var rankerTableView: UITableView!
    
    /* custom functions */
    func setRankerView() {
        /* Json 데이터를 파싱해서 유저 배열에 삽입 후, 정렬을 수행합니다. */
        self.m_tier = self.m_json["tier"] as! String
        self.m_name = self.m_json["name"] as! String
        self.m_entries = self.m_json["entries"] as! [[String:AnyObject]]
        
        self.appendAndSort()
    }
    func appendAndSort() {
        /* 유저 정보 Json에서 하나씩 유저 정보를 추출해 배열에 추가합니다. */
        for index in 0..<m_entries.count {
            var ranker = CRanker()
            ranker.summonerName = m_entries[index]["summonerName"] as! String
            ranker.leaguePoints = m_entries[index]["leaguePoints"] as! Int
            ranker.wins = m_entries[index]["wins"] as! Int
            ranker.losses = m_entries[index]["losses"] as! Int
            ranker.rank = m_entries[index]["rank"] as! String
            m_rankers.append(ranker)
        }
        
        // 리그 포인트 순 내림자순 정렬을 수행합니다.
        m_rankers = m_rankers.sorted(by: {$0.leaguePoints > $1.leaguePoints})
    }
    
    /* action functions */
    @IBAction func PushBack(_ sender: Any) {
        // 뒤로가기 버튼을 누르면 소환사 검색 뷰로 가는 세그웨이를 수행합니다.
        performSegue(withIdentifier: "backFromChallenger", sender: self)
    }
    
    /* override functions */
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setRankerView()
        
        /* ################################## For Debug */
        print("Scene 4 : Challenger List Scene")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return m_entries.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = rankerTableView.dequeueReusableCell(withIdentifier: "RankerCell") as! RankerTableViewCell
        
        cell.m_summonerNameLabel.text = m_rankers[indexPath.row].summonerName
        cell.m_leaguePointsLabel.text = "LP: \(m_rankers[indexPath.row].leaguePoints)"
        cell.m_tierLabel.text = m_tier + " " + m_rankers[indexPath.row].rank
        cell.m_winRateLabel.text = String(format: "승률: %.1f%%", ((Double(m_rankers[indexPath.row].wins) * 100.0)/Double(m_rankers[indexPath.row].wins + m_rankers[indexPath.row].losses)))
        
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
