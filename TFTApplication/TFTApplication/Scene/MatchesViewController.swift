//
//  MatchesViewController.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/06.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class MatchesViewController: UIViewController, UITableViewDataSource {
    
    /* member variables */
    var m_summoner : CSummoner!
    var m_matchIds : [String] = []
    //var m_matches : [CMatch] = []
    
    /* outlets */
    
    /* action functions */
    @IBAction func PushBack(_ sender: Any) {
        performSegue(withIdentifier: "backFromMatches", sender: self)
    }
    
    /* override functions */
    override func viewDidLoad() {
        super.viewDidLoad()

        /* ################################## For Debug */
        print("Scene 3 : Match List Scene")
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return m_matchIds.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value2, reuseIdentifier: nil)
        
        return cell
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
