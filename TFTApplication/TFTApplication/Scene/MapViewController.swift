//
//  MapViewController.swift
//  TFTApplication
//
//  Created by kpugame on 2020/06/25.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate, XMLParserDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    /* XML 관련 변수 */
    //  xml file을 download, parse하는 오브젝트
    var parser = XMLParser()
    //  feed 데이터를 저장하는 mutable array
    var post = NSMutableArray()
    //  title, date 같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()
    //  저장할 데이터 변수
    var title1 = NSMutableString()
    var title2 = NSMutableString()
    var xPos = NSMutableString()
    var yPos = NSMutableString()
    
    
    /* 파싱 함수 */
    func beginParsing() {
        post = []
        parser = XMLParser(contentsOf:(URL(string:"https://openapi.gg.go.kr/RegionMnyFacltStus?KEY=4f7070fb285a4bd4b65fa9f040440f17&pIndex=1&pSize=1000"))!)!
        parser.delegate = self
        parser.parse()
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        /* parser가 새로운 element를 발견하면 변수를 생성한다. */
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "row") {
            elements = NSMutableDictionary()
            elements = [:]
            title1 = NSMutableString()
            title1 = ""
            title2 = NSMutableString()
            title2 = ""
            xPos = NSMutableString()
            xPos = ""
            yPos = NSMutableString()
            yPos = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        /* title & pubDate을 발견하면 title1과 date에 완성한다. */
        if element.isEqual(to: "CMPNM_NM") {
            title1.append(string)
        } else if element.isEqual(to: "REFINE_ROADNM_ADDR") {
            title2.append(string)
        } else if element.isEqual(to: "REFINE_WGS84_LAT") {
            xPos.append(string)
        } else if element.isEqual(to: "REFINE_WGS84_LOGT") {
            yPos.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        /* element의 끄테서 feed데이터를 dictionary에 저장 */
        if (elementName as NSString).isEqual(to: "row") {
            if !title1.isEqual(nil) {
                elements.setObject(title1, forKey: "CMPNM_NM" as NSCopying)
            }
            if !title2.isEqual(nil) {
                elements.setObject(title2, forKey: "REFINE_ROADNM_ADDR" as NSCopying)
            }
            if !xPos.isEqual(nil) {
                elements.setObject(xPos, forKey: "REFINE_WGS84_LAT" as NSCopying)
            }
            if !yPos.isEqual(nil) {
                elements.setObject(yPos, forKey: "REFINE_WGS84_LOGT" as NSCopying)
            }
            post.add(elements)
        }
    }
    
    
    /* map 관련 변수 */
    // 이 지역은 regionRadius 의 거리에 따라 남북 및 동서에 걸쳐있을 것이다.
    let regionRadius : CLLocationDistance = 5000
    //  regionRadius 사용
    //  setRegion은 region으 표시하도록 mapView에 지시
    func centerMapOnLocation(location : CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //  hospital  objects array
    var myDatas : [MyData] = []
    
    //  전송받은 post 배열에서 정보를 얻어서 hospital object를 생성하고 배열에 추가 생성
    
    func loadInitialData() {
        for element in post {
            let CMPNM_NM = (element as AnyObject).value(forKey: "CMPNM_NM") as! NSString as String
            let REFINE_ROADNM_ADDR = (element as AnyObject).value(forKey: "REFINE_ROADNM_ADDR") as! NSString as String
            let REFINE_WGS84_LAT = (element as AnyObject).value(forKey: "REFINE_WGS84_LAT") as! NSString as String
            let REFINE_WGS84_LOGT = (element as AnyObject).value(forKey: "REFINE_WGS84_LOGT") as! NSString as String
            let lat = (REFINE_WGS84_LAT as NSString).doubleValue
            let lon = (REFINE_WGS84_LOGT as NSString).doubleValue
            let aData = MyData(title: CMPNM_NM, locationName: REFINE_ROADNM_ADDR, coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lon))
            myDatas.append(aData)
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        /* 사용자가 지도 주석 마커를 탭하면 설명 선에 info button이 표시됨
         info button을 누르면, mapView() 메서드가 호출된다.
         Hospital탭에서 참조하는 객체 항목을 만들고, 지도 항목을 MKMapItem 호출하여 지도 앱을 실행
         openInMaps(launchOptions:) 몇 가지 옵션을 지정할 수 있다. directionModeKey로 Driving을 설정
         이로 인해 지도 앱에서 사용자의 현재 위치에서 이 핀까지의 운전 경로를 표시할 수 있다. */
        let location = view.annotation as! MyData
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        /* mapView(:viewFor:)는 tableview(:cellForRowAt:)과 마찬가지로 지도에 추가하는 모든 주석이 호출되어 각 주석에 대한 보기를 반환. */
        
        //  이 주석(annotation)이 Hospital객체인지 확인, 아니면 nil 지도 뷰에서 기본 주석 뷰를 사용하도록 돌아간다.
        guard let annotation = annotation as? MyData else { return nil }
        
        //  마커가 나타나게 MKMarkerAnnotationView를 만든다. MKannotationView 마커 대신 이미지를 표시하는 객체를 만든다.
        let identifier = "marker"
        var view : MKMarkerAnnotationView
        
        //  코드를 새로 생성하기 전에 재사용 가능한 주석 뷰를 사용할 수 있는 지 먼저 확인한다.
        if let dequeueView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeueView.annotation = annotation
            view = dequeueView
        } else {
            //  MKMarkerAnnotationView 주석 보기에서 대기열에서 삭제할 수 없는 경우 여기에서 새 객체를 생성
            //  Hospital 클래스의 title, subtitle 속성을 사용하여 콜 아웃에 표시할 내용을 결정
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        beginParsing()
        
        // 초기 위치 서울시 광진구
        let initialLocation = CLLocation(latitude: 37.3402849, longitude: 126.7313189)
        
        centerMapOnLocation(location: initialLocation)
        mapView.delegate = self
        loadInitialData()
        mapView.addAnnotations(myDatas)
    }
    

}
