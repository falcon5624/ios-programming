//
//  ViewController.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    /* game variables */
    var player1 : Player = Player(name : "player1")
    var player2 : Player = Player(name : "player2")
    var player3 : Player = Player(name : "player3")
    
    var dealer : Player = Player(name : "dealer")
    
    var audioController : AudioController
    required init?(coder: NSCoder) {
        audioController = AudioController()
        audioController.preloadAudioEffects(audioFileNames: AudioEffectFiles)
        super.init(coder : coder)
    }
    
    var LCardsPlayer1 = [UIImageView]()
    var LCardsPlayer2 = [UIImageView]()
    var LCardsPlayer3 = [UIImageView]()
    var LCardsDealer = [UIImageView]()
    
    var deck = [Int]()
    var deckIndex = 0
    
    var nBetMoney1 : Int = 0
    var nBetMoney2 : Int = 0
    var nBetMoney3 : Int = 0
    var nPlayerMoney : Int = 1000
    var nCards : Int = 0 // 카드 놓는 위치 결정 변수
    var nState : Int = 0 // deal 버튼에 따른 턴 순서 변수
    
    var Hawtoo1 = [UILabel]() // p1
    var Hawtoo2 = [UILabel]() // p2
    var Hawtoo3 = [UILabel]() // p3
    var Hawtoo4 = [UILabel]() // dealer
    
    /* outlets */
    @IBOutlet weak var h10: UILabel!
    @IBOutlet weak var h11: UILabel!
    @IBOutlet weak var h12: UILabel!
    @IBOutlet weak var h13: UILabel!
    @IBOutlet weak var h14: UILabel!
    @IBOutlet weak var h20: UILabel!
    @IBOutlet weak var h21: UILabel!
    @IBOutlet weak var h22: UILabel!
    @IBOutlet weak var h23: UILabel!
    @IBOutlet weak var h24: UILabel!
    @IBOutlet weak var h30: UILabel!
    @IBOutlet weak var h31: UILabel!
    @IBOutlet weak var h32: UILabel!
    @IBOutlet weak var h33: UILabel!
    @IBOutlet weak var h34: UILabel!
    @IBOutlet weak var h40: UILabel!
    @IBOutlet weak var h41: UILabel!
    @IBOutlet weak var h42: UILabel!
    @IBOutlet weak var h43: UILabel!
    @IBOutlet weak var h44: UILabel!
    
    @IBOutlet weak var outletPlayerPts0: UILabel!
    @IBOutlet weak var outletPlayerPts1: UILabel!
    @IBOutlet weak var outletPlayerPts2: UILabel!
    @IBOutlet weak var outletDealerPts: UILabel!
    @IBOutlet weak var outletState1: UILabel!
    @IBOutlet weak var outletState2: UILabel!
    @IBOutlet weak var outletState3: UILabel!
    
    @IBOutlet weak var outletBetMoney1: UILabel!
    @IBOutlet weak var outletBetMoney2: UILabel!
    @IBOutlet weak var outletBetMoney3: UILabel!
    @IBOutlet weak var outletPlayerMoney: UILabel!
    
    @IBOutlet weak var outletBet15: UIButton!
    @IBOutlet weak var outletBet25: UIButton!
    @IBOutlet weak var outletBet35: UIButton!
    @IBOutlet weak var outletBet11: UIButton!
    @IBOutlet weak var outletBet21: UIButton!
    @IBOutlet weak var outletBet31: UIButton!
    
    @IBOutlet weak var outletDealButton: UIButton!
    @IBOutlet weak var outletAgainButton: UIButton!
    
    
    /* action functions */
    @IBAction func bet11(_ sender: Any) {
        nBetMoney1 += 10
        if nBetMoney1 <= nPlayerMoney {
            outletBetMoney1.text = "\(nBetMoney1)만"
            outletBetMoney1.sizeToFit()
            
            nPlayerMoney -= 10
            outletPlayerMoney.text = "\(nPlayerMoney)만"
            outletPlayerMoney.sizeToFit()
            
            outletBetMoney1.textAlignment = .right
            outletPlayerMoney.textAlignment = .right
            
            outletDealButton.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney1 -= 10
        }
    }
    @IBAction func bet21(_ sender: Any) {
        nBetMoney2 += 10
        if nBetMoney2 <= nPlayerMoney {
            outletBetMoney2.text = "\(nBetMoney2)만"
            outletBetMoney2.sizeToFit()
            
            nPlayerMoney -= 10
            outletPlayerMoney.text = "\(nPlayerMoney)만"
            outletPlayerMoney.sizeToFit()
            
            outletBetMoney2.textAlignment = .right
            outletPlayerMoney.textAlignment = .right
            
            outletDealButton.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney2 -= 10
        }
    }
    @IBAction func bet31(_ sender: Any) {
        nBetMoney3 += 10
        if nBetMoney3 <= nPlayerMoney {
            outletBetMoney3.text = "\(nBetMoney3)만"
            outletBetMoney3.sizeToFit()
            nPlayerMoney -= 10
            outletPlayerMoney.text = "\(nPlayerMoney)만"
            outletPlayerMoney.sizeToFit()
            
            outletBetMoney3.textAlignment = .right
            outletPlayerMoney.textAlignment = .right
            
            outletDealButton.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney3 -= 10
        }
    }
    @IBAction func bet15(_ sender: Any) {
        nBetMoney1 += 50
        if nBetMoney1 <= nPlayerMoney {
            outletBetMoney1.text = "\(nBetMoney1)만"
            outletBetMoney1.sizeToFit()
            nPlayerMoney -= 50
            outletPlayerMoney.text = "\(nPlayerMoney)만"
            outletPlayerMoney.sizeToFit()
            
            outletBetMoney1.textAlignment = .right
            outletPlayerMoney.textAlignment = .right
            
            outletDealButton.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney1 -= 50
        }
    }
    @IBAction func bet25(_ sender: Any) {
        nBetMoney2 += 50
        if nBetMoney2 <= nPlayerMoney {
            outletBetMoney2.text = "\(nBetMoney2)만"
            outletBetMoney2.sizeToFit()
            nPlayerMoney -= 50
            outletPlayerMoney.text = "\(nPlayerMoney)만"
            outletPlayerMoney.sizeToFit()
            
            outletBetMoney2.textAlignment = .right
            outletPlayerMoney.textAlignment = .right
            
            outletDealButton.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney2 -= 50
        }
    }
    @IBAction func bet35(_ sender: Any) {
        nBetMoney3 += 50
        if nBetMoney3 <= nPlayerMoney {
            outletBetMoney3.text = "\(nBetMoney3)만"
            outletBetMoney3.sizeToFit()
            nPlayerMoney -= 50
            outletPlayerMoney.text = "\(nPlayerMoney)만"
            outletPlayerMoney.sizeToFit()
            
            outletBetMoney3.textAlignment = .right
            outletPlayerMoney.textAlignment = .right
            
            outletDealButton.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        }
        else {
            nBetMoney3 -= 50
        }
    }
    
    @IBAction func deal(_ sender: Any) {
        if nState < 3 {
            self.gameDeal()
            nState += 1
        } else {
            return
        }
    }
    @IBAction func again(_ sender: Any) {
        nState = 0
        nBetMoney1 = 0
        nBetMoney2 = 0
        nBetMoney3 = 0
        
        player1.reset()
        player2.reset()
        player3.reset()
        dealer.reset()
        
        for card in LCardsDealer {
            card.removeFromSuperview()
        }
        for card in LCardsPlayer1 {
            card.removeFromSuperview()
        }
        for card in LCardsPlayer2 {
            card.removeFromSuperview()
        }
        for card in LCardsPlayer3 {
            card.removeFromSuperview()
        }
        
        LCardsDealer = [UIImageView]()
        LCardsPlayer1 = [UIImageView]()
        LCardsPlayer2 = [UIImageView]()
        LCardsPlayer3 = [UIImageView]()
        
        for i in 0..<5 {
            Hawtoo1[i].text = ""
            Hawtoo2[i].text = ""
            Hawtoo3[i].text = ""
            Hawtoo4[i].text = ""
            Hawtoo1[i].textColor = UIColor.white
            Hawtoo2[i].textColor = UIColor.white
            Hawtoo3[i].textColor = UIColor.white
            Hawtoo4[i].textColor = UIColor.white
        }
        outletPlayerPts0.text = ""
        outletPlayerPts1.text = ""
        outletPlayerPts2.text = ""
        outletDealerPts.text = ""
        outletState1.text = ""
        outletState2.text = ""
        outletState3.text = ""
    
        outletBetMoney1.text = "\(nBetMoney1)만"
        outletBetMoney2.text = "\(nBetMoney2)만"
        outletBetMoney3.text = "\(nBetMoney3)만"
        
        outletAgainButton.isEnabled = false
        outletDealButton.isEnabled = true
    }

    func checkWinner() {
        /* 딜러 카드 공개 */
        for index in 0..<5 {
            LCardsDealer[index].removeFromSuperview()
            let newImageView = UIImageView(image: UIImage(named: dealer.cards[index].filename())!)
            newImageView.center = CGPoint(x: 446.5 + Double(index) * 37.5, y: 192)
            
            self.view.addSubview(newImageView)
            LCardsDealer[index] = newImageView
            Hawtoo4[index].text = String(dealer.cards[index].getValue())
        }
        
        let player1Pts = self.Made(cards: player1.cards, lcards: self.LCardsPlayer1)
        let player2Pts = self.Made(cards: player2.cards, lcards: self.LCardsPlayer2)
        let player3Pts = self.Made(cards: player3.cards, lcards: self.LCardsPlayer3)
        let dealerPts = self.Made(cards: dealer.cards, lcards: self.LCardsDealer)
        
        outletPlayerPts0.text = player1Pts.0
        outletPlayerPts1.text = player2Pts.0
        outletPlayerPts2.text = player3Pts.0
        outletDealerPts.text = dealerPts.0
        
        if player1Pts.1 > dealerPts.1 {
            outletState1.text = "승"
            nPlayerMoney += nBetMoney1 * 2
        } else {
            outletState1.text = "패"
        }
        if player2Pts.1 > dealerPts.1 {
            outletState2.text = "승"
            nPlayerMoney += nBetMoney2 * 2
        } else {
            outletState2.text = "패"
        }
        if player3Pts.1 > dealerPts.1 {
            outletState3.text = "승"
            nPlayerMoney += nBetMoney3 * 2
        } else {
            outletState3.text = "패"
        }
        
        outletPlayerMoney.text = "\(nPlayerMoney)만"
        outletPlayerMoney.sizeToFit()
        outletPlayerMoney.textAlignment = .right
        
    }
    
    func drawP1(n: Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        player1.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 939 + 65.0/2.0, y: 594)
        
        self.view.addSubview(newImageView)
        
        Hawtoo1[n].text = String(newCard.getValue())
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 52.5 + Double(n) * 37.5, y: 510)
            newImageView.transform = CGAffineTransform(rotationAngle: 6.28)
        }, completion: nil)
        
        LCardsPlayer1.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func drawP2(n: Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        player2.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 939 + 65.0/2.0, y: 594)
        
        self.view.addSubview(newImageView)
        
        Hawtoo2[n].text = String(newCard.getValue())
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 347.5 + Double(n) * 37.5, y: 510)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsPlayer2.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func drawP3(n: Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        player3.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 939 + 65.0/2.0, y: 594)
        
        self.view.addSubview(newImageView)
        
        Hawtoo3[n].text = String(newCard.getValue())
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 583.5 + Double(n) * 37.5, y: 510)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsPlayer3.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func drawDealer(n: Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        dealer.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: "cardback")!)
        newImageView.center = CGPoint(x: 939 + 65.0/2.0, y: 594)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 446.5 + Double(n) * 37.5, y: 192)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsDealer.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }

    func gameDeal() {
        if nState == 0 {
            deck.shuffle()
            deckIndex = 0
            
            /* 1장 뽑고, 베팅 버튼 활성화 */
            outletBet11.isEnabled = true
            outletBet15.isEnabled = true
            outletBet21.isEnabled = true
            outletBet25.isEnabled = true
            outletBet31.isEnabled = true
            outletBet35.isEnabled = true
            
            drawP1(n: 0)
            drawP2(n: 0)
            drawP3(n: 0)
            drawDealer(n: 0)
            
        } else if nState == 1 {
            /* 카드 3장 뽑기 */
            for i in 1...3 {
                drawP1(n: i)
                drawP2(n: i)
                drawP3(n: i)
                drawDealer(n: i)
            }
        } else if nState == 2 {
            /* 마지막 1장 뽑고 승패 판정 */
            drawP1(n: 4)
            drawP2(n: 4)
            drawP3(n: 4)
            drawDealer(n: 4)
            
            self.checkWinner()
            outletAgainButton.isEnabled = true
            outletBet11.isEnabled = false
            outletBet15.isEnabled = false
            outletBet21.isEnabled = false
            outletBet25.isEnabled = false
            outletBet31.isEnabled = false
            outletBet35.isEnabled = false
        }
        outletDealButton.isEnabled = false
    }
    
    func Made(cards : [Card], lcards : [UIImageView]) -> (String, Int) {
        var valuelist : [Int] = []
        var valueCountlist : [Int] = [0,0,0,0,0,0,0,0,0,0]
        var retValue : String = "노 메이드"
        var point = -1
        for card in cards {
            valuelist.append(card.getValue())
        }
        
        for i in 0...4 {
            valueCountlist[valuelist[i] - 1] += 1
        }
        
        if valueCountlist[0] == 2 && valueCountlist[7] == 1 {
            retValue = "콩콩팔"
            let idx1 = valuelist.firstIndex(of: 1)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 1)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 8)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[0] == 1 && valueCountlist[1] == 1 && valueCountlist[6] == 1 {
            retValue = "삐리칠"
            let idx1 = valuelist.firstIndex(of: 1)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 2)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 7)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[0] == 1 && valueCountlist[2] == 1 && valueCountlist[5] == 1 {
            retValue = "물삼육"
            let idx1 = valuelist.firstIndex(of: 1)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 3)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 6)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[0] == 1 && valueCountlist[3] == 1 && valueCountlist[4] == 1 {
            retValue = "빽새오"
            let idx1 = valuelist.firstIndex(of: 1)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 4)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 5)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[0] == 1 && valueCountlist[8] == 1 && valueCountlist[9] == 1 {
            retValue = "삥구장"
            let idx1 = valuelist.firstIndex(of: 1)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 9)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 10)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[1] == 2 && valueCountlist[5] == 1 {
            retValue = "니니육"
            let idx1 = valuelist.firstIndex(of: 2)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 2)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 6)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[1] == 1 && valueCountlist[2] == 1 && valueCountlist[4] == 1 {
            retValue = "이삼오"
            let idx1 = valuelist.firstIndex(of: 2)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 3)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 5)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[1] == 1 && valueCountlist[7] == 1 && valueCountlist[9] == 1 {
            retValue = "이판장"
            let idx1 = valuelist.firstIndex(of: 2)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 8)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 10)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[2] == 2 && valueCountlist[3] == 1 {
            retValue = "심심새"
            let idx1 = valuelist.firstIndex(of: 3)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 3)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 4)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[2] == 1 && valueCountlist[6] == 1 && valueCountlist[9] == 1 {
            retValue = "삼칠장"
            let idx1 = valuelist.firstIndex(of: 3)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 7)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 10)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[2] == 1 && valueCountlist[7] == 1 && valueCountlist[8] == 1 {
            retValue = "삼빡구"
            let idx1 = valuelist.firstIndex(of: 3)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 8)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 9)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[3] == 2 && valueCountlist[1] == 1 {
            retValue = "살살이"
            let idx1 = valuelist.firstIndex(of: 4)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 4)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 2)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[3] == 1 && valueCountlist[5] == 1 && valueCountlist[9] == 1 {
            retValue = "사륙장"
            let idx1 = valuelist.firstIndex(of: 4)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 6)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 10)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[3] == 1 && valueCountlist[6] == 1 && valueCountlist[8] == 1 {
            retValue = "사칠구"
            let idx1 = valuelist.firstIndex(of: 4)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 7)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 9)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[4] == 2 && valueCountlist[9] == 1 {
            retValue = "꼬꼬장"
            let idx1 = valuelist.firstIndex(of: 5)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 5)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 10)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[4] == 1 && valueCountlist[5] == 1 && valueCountlist[8] == 1 {
            retValue = "오륙구"
            let idx1 = valuelist.firstIndex(of: 5)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 6)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 9)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[4] == 1 && valueCountlist[6] == 1 && valueCountlist[7] == 1 {
            retValue = "오리발"
            let idx1 = valuelist.firstIndex(of: 5)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 7)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 8)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[5] == 2 && valueCountlist[7] == 1 {
            retValue = "쭉쭉팔"
            let idx1 = valuelist.firstIndex(of: 6)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 6)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 8)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[6] == 2 && valueCountlist[5] == 1 {
            retValue = "철철육"
            let idx1 = valuelist.firstIndex(of: 7)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 7)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 6)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[7] == 2 && valueCountlist[3] == 1 {
            retValue = "팍팍싸"
            let idx1 = valuelist.firstIndex(of: 8)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 8)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 4)
            valuelist.remove(at: idx3!)
        }
        else if valueCountlist[8] == 2 && valueCountlist[1] == 1 {
            retValue = "구구리"
            let idx1 = valuelist.firstIndex(of: 9)
            valuelist.remove(at: idx1!)
            let idx2 = valuelist.firstIndex(of: 9)
            valuelist.remove(at: idx2!)
            let idx3 = valuelist.firstIndex(of: 2)
            valuelist.remove(at: idx3!)
        }
        
        if retValue != "노 메이드" {
            if valuelist.contains(3) && valuelist.contains(8) {
                retValue = retValue + " 38광땡"
                point = 1000
            } else if valuelist.contains(1) && valuelist.contains(8) || valuelist.contains(3) && valuelist.contains(1) {
                retValue = retValue + " 광땡"
                point = 999
            } else if valuelist[0] == 1 && valuelist[1] == 1 {
                retValue = retValue + " 1땡"
                point = 101
            } else if valuelist[0] == 2 && valuelist[1] == 2 {
                retValue = retValue + " 2땡"
                point = 102
            } else if valuelist[0] == 3 && valuelist[1] == 3 {
                retValue = retValue + " 3땡"
                point = 103
            } else if valuelist[0] == 4 && valuelist[1] == 4 {
                retValue = retValue + " 4땡"
                point = 104
            } else if valuelist[0] == 5 && valuelist[1] == 5 {
                retValue = retValue + " 5땡"
                point = 105
            } else if valuelist[0] == 6 && valuelist[1] == 6 {
                retValue = retValue + " 6땡"
                point = 106
            } else if valuelist[0] == 7 && valuelist[1] == 7 {
                retValue = retValue + " 7땡"
                point = 107
            } else if valuelist[0] == 8 && valuelist[1] == 8 {
                retValue = retValue + " 8땡"
                point = 108
            } else if valuelist[0] == 9 && valuelist[1] == 9 {
                retValue = retValue + " 9땡"
                point = 109
            } else if valuelist[0] == 10 && valuelist[1] == 10 {
                retValue = retValue + " 장땡"
                point = 110
            } else if valuelist.contains(2) && valuelist.contains(8) || valuelist.contains(3) && valuelist.contains(7) {
                retValue = retValue + " 망통"
                point = 0
            } else {
                point = (valuelist[0] + valuelist[1]) % 10
                if point > 0 && point < 10 {
                    retValue = retValue + " \(point)끝"
                } else {
                    retValue = retValue + " 망통"
                }
            }
        }
        
        return (retValue, point)
            
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        outletBet11.isEnabled = false
        outletBet15.isEnabled = false
        outletBet21.isEnabled = false
        outletBet25.isEnabled = false
        outletBet31.isEnabled = false
        outletBet35.isEnabled = false
        outletAgainButton.isEnabled = false
        
        for i in 0...39 {
            deck.append(i)
        }
        
        Hawtoo1 = [h10, h11, h12, h13, h14]
        Hawtoo2 = [h20, h21, h22, h23, h24]
        Hawtoo3 = [h30, h31, h32, h33, h34]
        Hawtoo4 = [h40, h41, h42, h43, h44]
                
        for i in 0..<5 {
            Hawtoo1[i].text = ""
            Hawtoo2[i].text = ""
            Hawtoo3[i].text = ""
            Hawtoo4[i].text = ""
            Hawtoo1[i].textColor = UIColor.white
            Hawtoo2[i].textColor = UIColor.white
            Hawtoo3[i].textColor = UIColor.white
            Hawtoo4[i].textColor = UIColor.white
        }
        outletPlayerPts0.text = ""
        outletPlayerPts1.text = ""
        outletPlayerPts2.text = ""
        outletDealerPts.text = ""
        outletState1.text = ""
        outletState2.text = ""
        outletState3.text = ""
        
        nBetMoney1 = 0
        nBetMoney2 = 0
        nBetMoney3 = 0
        outletBetMoney1.text = "\(nBetMoney1)만"
        outletBetMoney2.text = "\(nBetMoney2)만"
        outletBetMoney3.text = "\(nBetMoney3)만"
        nPlayerMoney = 1000
        outletPlayerMoney.textColor = UIColor.orange
        outletPlayerMoney.text = "\(nPlayerMoney)만"
    }

}


