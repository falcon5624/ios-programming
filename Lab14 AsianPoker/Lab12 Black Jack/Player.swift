//
//  Player.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class Player {
    var cards = [Card]()
    private var N : Int = 0
    private var name : String
    
    init(name : String) {
        self.name = name
    }
    func inHand() -> Int {
        return self.N
    }
    func getName() -> String {
        return self.name
    }
    func addCard(c: Card) {
        cards.append(c)
        self.N += 1
    }
    func reset() {
        self.N = 0
        cards.removeAll()
    }
}
