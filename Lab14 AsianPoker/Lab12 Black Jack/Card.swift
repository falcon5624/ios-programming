//
//  Card.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class Card {
    private var value : Int
    private var x : Int
    init(temp : Int) {
        self.value = temp % 10 + 1
        self.x = temp / 10 + 1
    }
    public func getValue() -> Int {
        return self.value
    }
    public func getX() -> Int {
        return self.x
    }
    func filename() -> String {
        return String(self.value) + ".\(self.x)"
    }
}
