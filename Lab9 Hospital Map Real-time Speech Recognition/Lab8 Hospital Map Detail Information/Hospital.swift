//
//  Hospital.swift
//  Lab8 Hospital Map Detail Information
//
//  Created by kpugame on 2020/05/19.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation
import MapKit


//  위치와 같은 주소, 도시 또는 상태 필드를 설정해야하는 경우
//  CNPostalAddressStreetKey와 같은 사전 키 상수가 포함
import Contacts

//  MKAnnotation 프로토콜을 구현하기 위해서 title, subtitle, coordinate 등이 필요
//  사용자가 핀을 선택할 때, title/subtitle을 표시
//  subtitle은 locationName을 반환하는 computed property

class Hospital : NSObject, MKAnnotation {
    let title: String?
    let locationName : String
    let coordinate: CLLocationCoordinate2D
    
    init(title : String, locationName : String, coordinate : CLLocationCoordinate2D) {
        self.title = title
        self.locationName = locationName
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        //  subtitle은 locationName을 반환하는 computed property
        return locationName
    }
    
    func mapItem() -> MKMapItem {
        /* 클래스에 추가하는 helper method
         MKPlacemark로 부터 MKMapItem을 생성, info button 누르면 MKMapItem 을 오프하게 된다. */
        let addressDict = [CNPostalAddressStreetKey : subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        return mapItem
    }
}
