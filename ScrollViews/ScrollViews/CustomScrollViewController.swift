//
//  CustomScrollViewController.swift
//  ScrollViews
//
//  Created by kpugame on 2020/04/22.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class CustomScrollViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet var scrollView : UIScrollView!
    var containerView : UIView!
    
    override func viewDidLoad() {
        
        //  4가지 뷰를 가지는 컨테이너 뷰 생성
        let containerSize = CGSize(width: 640.0, height: 640.0)
        containerView = UIView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: containerSize))
        scrollView.addSubview(containerView)
        
        //  RedView, 상단의 빨간 막대
        let redView = UIView(frame: CGRect(x: 0, y: 0, width: 640, height: 80))
        redView.backgroundColor = UIColor.red
        containerView.addSubview(redView)
        
        //  BlueView, 하단의 파란 막대
        let blueView = UIView(frame: CGRect(x: 0, y: 560, width: 640, height: 80))
        blueView.backgroundColor = UIColor.blue
        containerView.addSubview(blueView)
        
        //  GreenView, 가운데 녹색 사각형
        let greenView = UIView(frame: CGRect(x: 160, y: 160, width: 320, height: 320))
        greenView.backgroundColor = UIColor.green
        containerView.addSubview(greenView)
        
        //  imageView, slow.png를 담고 있음. 가운데에 위치
        let imageView = UIImageView(image: UIImage(named: "slow.png"))
        imageView.center = CGPoint(x: 320, y: 320)
        containerView.addSubview(imageView)
        
        //  스크롤 뷰 크기는 컨테이너 뷰 크기로 설정 (640, 640)
        scrollView.contentSize = containerSize
        
        //  minimum & maximum 줌 스케일 설정 : ViewController 에서 복사함
        //  최대 줌 아웃 시, 전체 이미지를 한 화면에 보기 위해 minimumZoomScale 설정
        //  width, height에 대하여 스크롤 프레임 크기 / 이미지 크기 의 값을 구하고 최소값으로 설정한다.
        //  예를 들어, 이미지가 화면 크기의 2배라면 minimumZoomScale을 0.5로 설정
        let scrollViewFrame = scrollView.frame
        let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
        let minimumScale = min(scaleWidth, scaleHeight)
        scrollView.minimumZoomScale = minimumScale
        
        //  MaximumZoomScale을 1 이상으로 설정하면 해상도보다 확대하므로 화질 저하됨
        //  초기 zoomScale은 minScale로 설정해서 한 화면에 이미지 전체를 볼 수 있도록 한다.
        scrollView.maximumZoomScale = 1.0
        scrollView.zoomScale = minimumScale
        
        //  스크롤 뷰 안에 이미지를 가운데 오도록 하는 helper 메서드 호출
        centerScrollViewContents()
    }
    
    func centerScrollViewContents() {
        /* containerView가 항상 가운데 오도록 설정 */
        let boundSize = scrollView.bounds.size
        var contentFrame = containerView.frame
        
        if contentFrame.size.width < boundSize.width {
            //  이미지가 화면 크기보다 작으면 이미지 origin(top, left) 위치를 가운데로 약간 이동 시킨다.
            contentFrame.origin.x = (boundSize.width - contentFrame.size.width) / 2.0
        } else {
            //  이미지가 화면보다 크면 무조건 (0,0)에서 시작한다.
            contentFrame.origin.x = 0.0
        }
        if contentFrame.size.height < boundSize.height {
            contentFrame.origin.y = (boundSize.height - contentFrame.size.height) / 2.0
        } else {
            contentFrame.origin.y = 0.0
        }
        containerView.frame = contentFrame
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return containerView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerScrollViewContents()
    }
}
