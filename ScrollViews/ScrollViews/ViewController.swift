//
//  ViewController.swift
//  ScrollViews
//
//  Created by kpugame on 2020/04/20.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet var scrollView : UIScrollView!
    var imageView : UIImageView!
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        centerScrollViewContents()
    }
    
    func centerScrollViewContents() {
        let boundSize = scrollView.bounds.size
        var contentFrame = imageView.frame
        
        if contentFrame.size.width < boundSize.width {
            //  이미지가 화면 크기보다 작으면 이미지 origin(top, left) 위치를 가운데로 약간 이동 시킨다.
            contentFrame.origin.x = (boundSize.width - contentFrame.size.width) / 2.0
        } else {
            //  이미지가 화면보다 크면 무조건 (0,0)에서 시작한다.
            contentFrame.origin.x = 0.0
        }
        if contentFrame.size.height < boundSize.height {
            contentFrame.origin.y = (boundSize.height - contentFrame.size.height) / 2.0
        } else {
            contentFrame.origin.y = 0.0
        }
        imageView.frame = contentFrame
    }
    
    //  더블 탭으로 줌 인하는 메서드
    @objc func scrollViewDoubleTapped(_ recognizer : UITapGestureRecognizer) {
        //  줌 인 위치는 이미지 안의 탭한 위치로 설정
        let tappedPoint = recognizer.location(in: imageView)
        
        //  150% 줌 인하는 데 최대 maximumZoomScale 까지만 줌 인하도록
        var newZoomScale = scrollView.zoomScale * 1.5
        newZoomScale = min(newZoomScale, scrollView.maximumZoomScale)
        
        //  시작 위치와 줌 인한 width, height에 의해서 CGRect 생성
        let scrollViewSize = scrollView.bounds.size
        let w = scrollViewSize.width / newZoomScale
        let h = scrollViewSize.height / newZoomScale
        let x = tappedPoint.x - (w / 2.0)
        let y = tappedPoint.y - (h / 2.0)
        
        let rectToZoomTo = CGRect(x: x, y: y, width: w, height: h)
        
        //  Zoom in
        scrollView.zoom(to: rectToZoomTo, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  이미지 프레임(크기, 위치)을 설정한 후 scrollView의 subview로 추가한다.
        let image = UIImage(named: "photo1.png")!
        imageView = UIImageView(image: image)
        imageView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: image.size)
        scrollView.addSubview(imageView)
        
        //  수직 수평으로 스크롤하는 컨텐트의 크기를 이미지의 크기로 설정한다.
        scrollView.contentSize = image.size
        
        //  줌 인 하기 위하여 doubleTapRecognizer 설정
        var doubleTapRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.scrollViewDoubleTapped(_:)))
        doubleTapRecognizer.numberOfTapsRequired = 2
        doubleTapRecognizer.numberOfTouchesRequired = 1
        scrollView.addGestureRecognizer(doubleTapRecognizer)
        
        //  최대 줌 아웃 시, 전체 이미지를 한 화면에 보기 위해 minimumZoomScale 설정
        //  width, height에 대하여 스크롤 프레임 크기 / 이미지 크기 의 값을 구하고 최소값으로 설정한다.
        //  예를 들어, 이미지가 화면 크기의 2배라면 minimumZoomScale을 0.5로 설정
        let scrollViewFrame = scrollView.frame
        let scaleWidth = scrollViewFrame.size.width / scrollView.contentSize.width
        let scaleHeight = scrollViewFrame.size.height / scrollView.contentSize.height
        let minimumScale = min(scaleWidth, scaleHeight)
        scrollView.minimumZoomScale = minimumScale
        
        //  MaximumZoomScale을 1 이상으로 설정하면 해상도보다 확대하므로 화질 저하됨
        //  초기 zoomScale은 minScale로 설정해서 한 화면에 이미지 전체를 볼 수 있도록 한다.
        scrollView.maximumZoomScale = 1.0
        scrollView.zoomScale = minimumScale
        
        //  스크롤 뷰 안에 이미지를 가운데 오도록 하는 helper 메서드 호출
        centerScrollViewContents()
    }
}

