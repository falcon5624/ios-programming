//
//  Artwork.swift
//  Mapkit Tutorial
//
//  Created by kpugame on 2020/05/23.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation
import MapKit

class Artwork : NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D
    
    let m_sTitle : String?
    let m_sLocationName : String?
    let m_sDiscipline : String?
    let m_cllc2DCoordinate : CLLocationCoordinate2D
    
    init(title : String, locationName : String, discipline : String, location : CLLocationCoordinate2D) {
        self.m_sTitle = title
        self.m_sLocationName = locationName
        self.m_sDiscipline = discipline
        self.m_cllc2DCoordinate = location
        
        super.init()
    }
    
    var subtitle : String? {
        return m_sLocationName
    }
}
