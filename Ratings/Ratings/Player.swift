//
//  Player.swift
//  Ratings
//
//  Created by kpugame on 2020/04/15.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class Player {
    var name : String?
    var game : String?
    var rating : Int
    
    init(name : String?, game : String?, rating : Int) {
        self.name = name; self.game = game; self.rating = rating
    }
}
