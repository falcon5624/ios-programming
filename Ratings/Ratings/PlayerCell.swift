//
//  PlayerCell.swift
//  Ratings
//
//  Created by kpugame on 2020/04/15.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class PlayerCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel : UILabel!
    @IBOutlet weak var gameLabel : UILabel!
    @IBOutlet weak var ratingImageView : UIImageView!
    
    /* 일반 함수 */
    func SetImageAsRating(rating : Int) -> UIImage? {
        let imageName = "\(rating)Stars" //  이미지 이름 규격이 'n'Stars로 되어있음.
        return UIImage(named: imageName)
    }
    
    var player : Player! {
        didSet {
            nameLabel.text = player.name
            gameLabel.text = player.game
            ratingImageView.image = self.SetImageAsRating(rating: player.rating)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
