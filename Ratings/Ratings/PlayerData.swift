//
//  PlayerData.swift
//  Ratings
//
//  Created by kpugame on 2020/04/15.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

//  Set up Player Data
let a_playersData = [
    Player(name: "Bill Evans", game: "Tic-Tac-Toe", rating: 4),
    Player(name: "Oscar Peterson", game: "Spin the Bottle", rating: 5),
    Player(name: "Dave Brubeck", game: "Texas Hold 'em Poker", rating: 2)
]
