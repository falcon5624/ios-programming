//
//  PlayerDetailsViewController.swift
//  Ratings
//
//  Created by kpugame on 2020/04/15.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class PlayerDetailsViewController: UITableViewController {

    /* IBOulet 변수들 */
    @IBOutlet weak var playerNameTextField: UITextField!
    @IBOutlet weak var gameDetailLabel: UILabel!
    
    /* 일반 변수 */
    var player : Player?
    var game : String = "월드 오브 워크래프트" {
        didSet {
            gameDetailLabel.text? = game
        }
    }
    
    @IBAction func UnwindWithSelectedGame(segue :  UIStoryboardSegue) {
        if let gamePickerViewController = segue.source as? GamePickerViewController,
            let selectedGame = gamePickerViewController.selectedGame {
            game = selectedGame
        }
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            playerNameTextField.becomeFirstResponder()
        }
    }
    
    //  prepare 함수는 반드시 오버라이드 해주어야 한다. 즉, 함수 이름이 정해져 있다.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SavePlayerDetail" {
            player = Player(name: playerNameTextField.text!, game: game, rating: 5)
        }
        
        //  장면 전환 이후에도, 체크마크를 남기기 위해서
        if segue.identifier == "PickGame" {
            if let gamePickerViewController = segue.destination as? GamePickerViewController{
                gamePickerViewController.selectedGame = game
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
}
