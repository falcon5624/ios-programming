//
//  HospitalTableViewController.swift
//  Lab8 Hospital Map Detail Information
//
//  Created by kpugame on 2020/05/19.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class HospitalTableViewController: UITableViewController, XMLParserDelegate {
    
    /* XML 관련 변수 */
    //  xml file을 download, parse하는 오브젝트
    var parser = XMLParser()
    //  feed 데이터를 저장하는 mutable array
    var post = NSMutableArray()
    //  title, date 같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()
    //  저장 문자열 변수
    var yadmNm = NSMutableString()
    var addr = NSMutableString()
    var xPos = NSMutableString()
    var yPos = NSMutableString()
    //  new url variable
    var hospitalname = ""
    var hospitalname_utf8 = ""
    
    /* Outlets */
    @IBOutlet var tableData: UITableView!
    
    //  viewController로 부터 전달받은 url
    var url : String?
    
    /* 파싱 함수  */
    func beginParsing() {
        post = []
        parser = XMLParser(contentsOf: (URL(string: url!))!)!
        parser.delegate = self
        parser.parse()
        tableData!.reloadData()
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        /* parser가 새로운 element를 발견하면 변수를 생성한다. */
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item") {
            elements = NSMutableDictionary()
            elements = [:]
            yadmNm = NSMutableString()
            yadmNm = ""
            addr = NSMutableString()
            addr = ""
            xPos = NSMutableString()
            xPos = ""
            yPos = NSMutableString()
            yPos = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        /* 병원이름과 주소를 발견하면 yadmNm과 addr에 완성 */
        if element.isEqual(to: "yadmNm") {
            yadmNm.append(string)
        } else if element.isEqual(to: "addr") {
            addr.append(string)
        } else if element.isEqual(to: "XPos") {
            xPos.append(string)
        } else if element.isEqual(to: "YPos") {
            yPos.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        /* element의 끝에서 feed데이터를 dictionary에 저장 */
        if (elementName as NSString).isEqual(to: "item") {
            if !yadmNm.isEqual(nil) {
                elements.setObject(yadmNm, forKey: "yadmNm" as NSCopying)
            }
            if !addr.isEqual(nil) {
                elements.setObject(addr, forKey: "addr" as NSCopying)
            }
            if !xPos.isEqual(nil) {
                elements.setObject(xPos, forKey: "XPos" as NSCopying)
            }
            if !yPos.isEqual(nil) {
                elements.setObject(yPos, forKey: "YPos" as NSCopying)
            }
            post.add(elements)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return post.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        cell.textLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "yadmNm") as! NSString as String
        cell.detailTextLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "addr") as! NSString as String

        return cell
    }
    
    /* prepare method */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToMapView" {
            if let mapViewController = segue.destination as? MapViewController {
                mapViewController.post = post
            }
        }
        
        if segue.identifier == "segueToHospitalDetail" {
            if let cell = sender as? UITableViewCell {
                let indexPath = tableView.indexPath(for: cell)
                hospitalname = (post.object(at: (indexPath?.row)!) as AnyObject).value(forKey: "yadmNm") as! NSString as String
                //  url에서 한글을 쓸 수 있도록 코딩한다.
                hospitalname_utf8 = hospitalname.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
                //  선택한 row의 병원명을 추가하여 다시 url을 구성하여 넘겨준다.
                if let hospitalDetailTableViewController = segue.destination as? HospitalDetailTableViewController {
                    hospitalDetailTableViewController.url = url! + "&yadmNm=" + hospitalname_utf8
                }
            }
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
