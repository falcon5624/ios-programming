//
//  MapViewController.swift
//  Lab8 Hospital Map Detail Information
//
//  Created by kpugame on 2020/05/19.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController, MKMapViewDelegate {
    
    /* outlets */
    @IBOutlet var mapView: MKMapView!
    
    // feed data 를 저장하는 mutable array
    var post = NSMutableArray()
    
    /* map 관련 변수 */
    // 이 지역은 regionRadius 의 거리에 따라 남북 및 동서에 걸쳐있을 것이다.
    let regionRadius : CLLocationDistance = 5000
    //  regionRadius 사용
    //  setRegion은 region으 표시하도록 mapView에 지시
    func centerMapOnLocation(location : CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    //  hospital  objects array
    var hospitals : [Hospital] = []
    
    //  전송받은 post 배열에서 정보를 얻어서 hospital object를 생성하고 배열에 추가 생성
    
    func loadInitialData() {
        for element in post {
            let yadmNm = (element as AnyObject).value(forKey: "yadmNm") as! NSString as String
            let addr = (element as AnyObject).value(forKey: "addr") as! NSString as String
            let xPos = (element as AnyObject).value(forKey: "XPos") as! NSString as String
            let yPos = (element as AnyObject).value(forKey: "YPos") as! NSString as String
            let lat = (yPos as NSString).doubleValue
            let lon = (xPos as NSString).doubleValue
            let hospital = Hospital(title: yadmNm, locationName: addr, coordinate: CLLocationCoordinate2D(latitude: lat, longitude: lon))
            hospitals.append(hospital)
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        /* 사용자가 지도 주석 마커를 탭하면 설명 선에 info button이 표시됨
         info button을 누르면, mapView() 메서드가 호출된다.
         Hospital탭에서 참조하는 객체 항목을 만들고, 지도 항목을 MKMapItem 호출하여 지도 앱을 실행
         openInMaps(launchOptions:) 몇 가지 옵션을 지정할 수 있다. directionModeKey로 Driving을 설정
         이로 인해 지도 앱에서 사용자의 현재 위치에서 이 핀까지의 운전 경로를 표시할 수 있다. */
        let location = view.annotation as! Hospital
        let launchOptions = [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving]
        location.mapItem().openInMaps(launchOptions: launchOptions)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        /* mapView(:viewFor:)는 tableview(:cellForRowAt:)과 마찬가지로 지도에 추가하는 모든 주석이 호출되어 각 주석에 대한 보기를 반환. */
        
        //  이 주석(annotation)이 Hospital객체인지 확인, 아니면 nil 지도 뷰에서 기본 주석 뷰를 사용하도록 돌아간다.
        guard let annotation = annotation as? Hospital else { return nil }
        
        //  마커가 나타나게 MKMarkerAnnotationView를 만든다. MKannotationView 마커 대신 이미지를 표시하는 객체를 만든다.
        let identifier = "marker"
        var view : MKMarkerAnnotationView
        
        //  코드를 새로 생성하기 전에 재사용 가능한 주석 뷰를 사용할 수 있는 지 먼저 확인한다.
        if let dequeueView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            dequeueView.annotation = annotation
            view = dequeueView
        } else {
            //  MKMarkerAnnotationView 주석 보기에서 대기열에서 삭제할 수 없는 경우 여기에서 새 객체를 생성
            //  Hospital 클래스의 title, subtitle 속성을 사용하여 콜 아웃에 표시할 내용을 결정
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        return view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // 초기 위치 서울시 광진구
        let initialLocation = CLLocation(latitude: 37.5384514, longitude: 127.0709764)
        
        centerMapOnLocation(location: initialLocation)
        mapView.delegate = self
        loadInitialData()
        mapView.addAnnotations(hospitals)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
