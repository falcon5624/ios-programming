//
//  ViewController.swift
//  Lab8 Hospital Map Detail Information
//
//  Created by kpugame on 2020/05/19.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    /* 프로토콜 함수 */
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        /* pickerView의 컴포넌트 개수 = 1 */
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        /* pickerView의 각 컴포넌트에 대한  row의 개수 = pcikerDataSource 배열 원소의 개수 */
        return m_pickerDataSource.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        /* pickerView의 주어진 컴포넌트/row에 대한 데이터 = pickerDataSource */
        return m_pickerDataSource[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch row {
        case 0:
            sgguCd = "110023"   //  광진구 시구코드
        case 1:
            sgguCd = "110005"   //  구로구 시구코드
        case 2:
            sgguCd = "110007"   //  동대문구 시구코드
        case 3:
            sgguCd = "110016"   //  종로구 시구코드
        default:
            sgguCd = "110023"
        }
    }
    
    /* segue func */
    @IBAction func doneToPickerViewController(segue : UIStoryboardSegue) {
        /* Done 버튼을 누르면 동작하는 unwind 메소드 */
        /* 아무 동작도 하지 않지만 이 메소드가 있어야 HospitalTableView에서 unwind 연결이 가능하다. */
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        /* prepare는 segue 실행될 때 호출되는 메소드 */
        /* HospitalTableView에 url 정보를 전달하기 위해 */
        /* 먼저, Navigation Controller를 destination으로 설정해 HospitalTableView를 찾아내 선택 */
        if segue.identifier == "segueToHospitalTableView" {
            if let navController = segue.destination as? UINavigationController {
                if let hospitalTableViewController = navController.topViewController as? HospitalTableViewController {
                    hospitalTableViewController.url = url + sgguCd
                }
            }
        }
    }
    
    /* Outlets */
    @IBOutlet weak var pickerView: UIPickerView!
    
    //  pickerView  Data Array
    var m_pickerDataSource = ["광진구", "구로구", "동대문구", "종로구"]
    
    //보훈병원정보 OpenAPI 및 인증키
    //디폴트 시도코드 = 서울 (sideCd=110000)
    //ServiceKey = "sea100UMmw23Xycs33F1EQnumONR%2F9ElxBLzkilU9Yr1oT4TrCot8Y2p0jyuJP72x9rG9D8CN5yuEs6AS2sAiw%3D%3D"
    var apiKey = "4btacSdDzipkrK%2F8IcvCJYi8xEKDeB8am4DQrRORVaV2t0NTwVbuw3LTtKg0AQlHjJnNjYe%2BFkGZfzuWHOxf1g%3D%3D"
    var url : String = "http://apis.data.go.kr/B551182/hospInfoService/getHospBasisList?pageNo=1&numOfRows=10&serviceKey=4btacSdDzipkrK%2F8IcvCJYi8xEKDeB8am4DQrRORVaV2t0NTwVbuw3LTtKg0AQlHjJnNjYe%2BFkGZfzuWHOxf1g%3D%3D&sidoCd=110000&sgguCd="
    var sgguCd : String = "110023" //   디폴트 시구코드 = 광진구
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pickerView.delegate = self
        self.pickerView.dataSource = self
    }


}

