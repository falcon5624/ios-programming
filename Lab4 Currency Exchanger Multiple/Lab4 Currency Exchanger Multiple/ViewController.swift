//
//  ViewController.swift
//  Lab4 Currency Exchanger Multiple
//
//  Created by kpugame on 2020/04/14.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue : Int = 0
        if tableView == USDTableView {
            returnValue = a_USDIndex.count
        } else if tableView == EURTableView {
            returnValue = a_EURIndex.count
        } else {
            returnValue = a_JPYIndex.count
        }
        return returnValue
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value2, reuseIdentifier: nil)
        var possbileMoney : Int
        var couponPercentage : Int
        
        //  해당되는 뷰에 맞게 데이터 처리
        if tableView == USDTableView {
            couponPercentage = a_USDIndex[indexPath.row]
            possbileMoney = dic_possibleUSD[couponPercentage]!
            cell.textLabel?.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: CGFloat(couponPercentage) / 100.0 + 0.4)
            cell.detailTextLabel?.text = String(format:"%d $ 환전 가능!", possbileMoney)
            cell.backgroundColor = UIColor(red: CGFloat(couponPercentage) / 100.0, green: 0.0, blue: 0.0, alpha: 1)
            
        } else if tableView == EURTableView {
            couponPercentage = a_EURIndex[indexPath.row]
            possbileMoney = dic_possibleEUR[couponPercentage]!
            cell.textLabel?.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: CGFloat(couponPercentage) / 100.0 + 0.4)
            cell.detailTextLabel?.text = String(format:"%d € 환전 가능!", possbileMoney)
            cell.backgroundColor = UIColor(red: 0.0, green: CGFloat(couponPercentage) / 100.0, blue: 0.0, alpha: 1)
            
        } else {
            couponPercentage = a_JPYIndex[indexPath.row]
            possbileMoney = dic_possibleJPY[couponPercentage]!
            cell.textLabel?.textColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: CGFloat(couponPercentage) / 100.0 + 0.4)
            cell.detailTextLabel?.text = String(format:"%d ¥ 환전 가능!", possbileMoney)
            cell.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: CGFloat(couponPercentage) / 100.0, alpha: 1)
        }
        //  공통된 cell 속성 변화
        cell.textLabel?.text = "\(couponPercentage)% 쿠폰:"
        cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 25)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.detailTextLabel?.textColor = UIColor.white
        cell.detailTextLabel?.font = UIFont.boldSystemFont(ofSize: 22)
        
        return cell
    }
    

    /* 아울렛 변수 선언 */
    @IBOutlet weak var originalMoneyLabel: UILabel!
    @IBOutlet weak var originalMoneyTextField: UITextField!
    
    @IBOutlet weak var exchangeRateUSDLabel: UILabel!
    @IBOutlet weak var exchangeRateUSDSlider: UISlider!
    
    @IBOutlet weak var exchangeRateEURLabel: UILabel!
    @IBOutlet weak var exchangeRateEURSlider: UISlider!
    
    @IBOutlet weak var exchangeRateJPYLabel: UILabel!
    @IBOutlet weak var exchangeRateJPYSlider: UISlider!
    
    @IBOutlet weak var USDTableView: UITableView!
    @IBOutlet weak var EURTableView: UITableView!
    @IBOutlet weak var JPYTableView: UITableView!
    
    @IBOutlet weak var calculateButton: UIButton!
    
    /* 일반 변수 선언 */
    var timer = Timer()                     //  애니메이션을 위한 타이머 객체
    var f_buttonAlplaValue : Float = 1.0    //  버튼의 투명도 값
    var b_isIncreasing : Bool = false       //  버튼의 투명도 변화 방향
    let f_frametime : Float = 1.0/60.0      //  애니메이션 프레임 시간
    var a_USDIndex : [Int] = []
    var a_EURIndex : [Int] = []
    var a_JPYIndex : [Int] = []
    var dic_possibleUSD : Dictionary<Int, Int> = Dictionary<Int, Int>()
    var dic_possibleEUR : Dictionary<Int, Int> = Dictionary<Int, Int>()
    var dic_possibleJPY : Dictionary<Int, Int> = Dictionary<Int, Int>()
    
    //  환전 계산기 클래스 인스턴스
    let ccm : CurrencyCalculatorMultiple = CurrencyCalculatorMultiple(originalMoney: 1000, exchangeRateUSD: 1214.97, exchangeRateEUR: 1332.55, exchangeRateJPY: 11.32)
    
    /* 액션 함수 선언 */
    @IBAction func Calculate(_ sender: UIButton) {
        /* 계산하기 버튼을 누르면 동작 */
        ccm.originalMoney = Float((originalMoneyTextField.text! as NSString).intValue)
        dic_possibleUSD = ccm.returnPossibleMoney(flag: .USD)
        dic_possibleEUR = ccm.returnPossibleMoney(flag: .EUR)
        dic_possibleJPY = ccm.returnPossibleMoney(flag: .JPY)
        a_USDIndex = Array(dic_possibleUSD.keys).sorted()
        a_EURIndex = Array(dic_possibleEUR.keys).sorted()
        a_JPYIndex = Array(dic_possibleJPY.keys).sorted()
        USDTableView.reloadData()
        EURTableView.reloadData()
        JPYTableView.reloadData()
    }
    
    @IBAction func ChangeUSDRate(_ sender: UISlider) {
        ccm.exchangeRateUSD = exchangeRateUSDSlider.value
        self.RefreshUI()
    }
    @IBAction func ChangeEURRate(_ sender: UISlider) {
        ccm.exchangeRateEUR = exchangeRateEURSlider.value
        self.RefreshUI()
    }
    @IBAction func ChangeJPYRate(_ sender: UISlider) {
        ccm.exchangeRateJPY = exchangeRateJPYSlider.value
        self.RefreshUI()
    }
    
    
    /* 타이머 함수 선언 */
    @objc func Update() {
        /* 이 함수에서 계산하기 버튼의 숨쉬기 효과를 구현합니다. */
        //  알파값 초기화, 방향 전환
        if f_buttonAlplaValue > 1.0 {
            f_buttonAlplaValue = 1.0
            b_isIncreasing = !b_isIncreasing
        }
        if f_buttonAlplaValue < 0.0 {
            f_buttonAlplaValue = 0.0
            b_isIncreasing = !b_isIncreasing
        }
        
        //  알파값 증가
        if (b_isIncreasing) {
            f_buttonAlplaValue += f_frametime
        } else {
            f_buttonAlplaValue -= f_frametime
        }
        
        //  버튼 텍스트 색상 변경
        calculateButton.titleLabel?.textColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: CGFloat(f_buttonAlplaValue))
    }
    
    /* 일반 함수 선언 */
    func SetTimer() {
        /* 타이머 설정 함수 */
        timer = Timer.scheduledTimer(timeInterval: TimeInterval(f_frametime), target: self, selector: #selector(self.Update), userInfo: nil, repeats: true)
    }
    func RefreshUI() {
        /* UI를 새로고침합니다. */
        //  라벨 부분
        originalMoneyTextField.text = String(format: "%d", Int(ccm.originalMoney))
        exchangeRateUSDLabel.text = String(format : "%.2f", ccm.exchangeRateUSD) + " ₩/$:"
        exchangeRateEURLabel.text = String(format : "%.2f", ccm.exchangeRateEUR) + " ₩/€:"
        exchangeRateJPYLabel.text = String(format : "%.2f", ccm.exchangeRateJPY) + " ₩/¥:"
        
        //  슬라이더 부분
        let defaultValue : CGFloat = 0.0
        exchangeRateUSDSlider.value = ccm.exchangeRateUSD
        let redValue : CGFloat = CGFloat((exchangeRateUSDSlider.value - exchangeRateUSDSlider.minimumValue) / (exchangeRateUSDSlider.maximumValue - exchangeRateUSDSlider.minimumValue))
        exchangeRateUSDSlider.tintColor = UIColor(red: redValue + 0.1, green: defaultValue, blue: defaultValue, alpha: 1.0)
        
        exchangeRateEURSlider.value = ccm.exchangeRateEUR
        let greenValue : CGFloat = CGFloat((exchangeRateEURSlider.value - exchangeRateEURSlider.minimumValue) / (exchangeRateEURSlider.maximumValue - exchangeRateEURSlider.minimumValue))
        exchangeRateEURSlider.tintColor = UIColor(red: defaultValue, green: greenValue + 0.1, blue: defaultValue, alpha: 1.0)
        
        exchangeRateJPYSlider.value = ccm.exchangeRateJPY
        let blueValue : CGFloat = CGFloat((exchangeRateJPYSlider.value - exchangeRateJPYSlider.minimumValue) / (exchangeRateJPYSlider.maximumValue - exchangeRateJPYSlider.minimumValue))
        exchangeRateJPYSlider.tintColor = UIColor(red: defaultValue, green: defaultValue, blue: blueValue + 0.1, alpha: 1.0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SetTimer()
        RefreshUI()
    }
}

