//
//  Currency Calculator Multiple.swift
//  Lab4 Currency Exchanger Multiple
//
//  Created by kpugame on 2020/04/14.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

enum exchangeFlag : Int {
    /* Calculate 함수의 연산 방법을 결정하는 신호값 */
    case USD = 0
    case EUR = 1
    case JPY = 2
}

class CurrencyCalculatorMultiple {
    
    var originalMoney : Float       //  원화 총액
    var exchangeRateUSD : Float     //  원/달러 환율, [1000.0, 1300.0]
    var exchangeRateEUR : Float     //  원/유로 환율, [1100.0, 1400.0]
    var exchangeRateJPY : Float     //  원/엔 환율, [8.0, 12.0]
    var exchangeFee : Float = 0.02  //  환전 수수료
    
    init(originalMoney : Int, exchangeRateUSD : Float, exchangeRateEUR : Float, exchangeRateJPY : Float) {
        self.originalMoney = Float(originalMoney); self.exchangeRateUSD = exchangeRateUSD; self.exchangeRateEUR = exchangeRateEUR; self.exchangeRateJPY = exchangeRateJPY
    }
    
    func Calculate(preferentialCouponPercentage : Float /* 퍼센트값 */, flag : exchangeFlag) -> Int {
        /* 이 함수에서 입력 받은 쿠폰값으로 환전 수수료를 계산해 환전 금액을 반환합니다. */
        var returnValue : Float = 0.0
        let feeTotal : Float = self.exchangeFee * (1 - preferentialCouponPercentage / 100.0)
        var exchangeRate : Float = 0.0
        
        //  환율 계산 대상 국가를 정합니다.
        switch flag {
        case .USD:
            exchangeRate = self.exchangeRateUSD
            break;
            
        case .EUR:
            exchangeRate = self.exchangeRateEUR
            break;
            
        case .JPY:
            exchangeRate = self.exchangeRateJPY
            break;
        }
        
        //  환전 금액 계산식
        returnValue = self.originalMoney / (exchangeRate * (1 + feeTotal))
        
        return Int(returnValue)
    }
    
    func returnPossibleMoney(flag : exchangeFlag) -> Dictionary<Int, Int> {
        /* 이 함수에서 쿠폰 마다 환전 가능한 금액을 유효한 데이터 집합체로 반환합니다. */
        let couponList = [30, 50, 70]
        var returnValue : Dictionary<Int, Int> = Dictionary<Int, Int>()
        
        for coupon in couponList {
            returnValue[coupon] = self.Calculate(preferentialCouponPercentage: Float(coupon), flag: flag)
        }
        
        return returnValue
    }
}
