//
//  ViewController.swift
//  XMLParsingDemo
//
//  Created by kpugame on 2020/05/19.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController_Korea: UIViewController, XMLParserDelegate , UITableViewDataSource {
    
    /* outlets */
    @IBOutlet weak var tableData: UITableView!
    
    /* XML 관련 변수 */
    //  xml file을 download, parse하는 오브젝트
    var parser = XMLParser()
    //  feed 데이터를 저장하는 mutable array
    var post = NSMutableArray()
    //  title, date 같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()
    //  저장할 데이터 변수
    var title1 = NSMutableString()
    var date = NSMutableString()
    var imageURL = NSMutableString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
    }
    
    /* 파싱 함수 */
    func beginParsing() {
        post = []
        parser = XMLParser(contentsOf:(URL(string:"http://apis.data.go.kr/9710000/NationalAssemblyInfoService/getMemberCurrStateList?ServiceKey=4btacSdDzipkrK%2F8IcvCJYi8xEKDeB8am4DQrRORVaV2t0NTwVbuw3LTtKg0AQlHjJnNjYe%2BFkGZfzuWHOxf1g%3D%3D"))!)!
        parser.delegate = self
        parser.parse()
        tableData!.reloadData()
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        /* parser가 새로운 element를 발견하면 변수를 생성한다. */
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item") {
            elements = NSMutableDictionary()
            elements = [:]
            title1 = NSMutableString()
            title1 = ""
            date = NSMutableString()
            date = ""
            imageURL = NSMutableString()
            imageURL = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        /* title & pubDate을 발견하면 title1과 date에 완성한다. */
        if element.isEqual(to: "empNm") {
            title1.append(string)
        } else if element.isEqual(to: "origNm") {
            date.append(string)
        } else if element.isEqual(to: "jpgLink") {
            imageURL.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        /* element의 끄테서 feed데이터를 dictionary에 저장 */
        if (elementName as NSString).isEqual(to: "item") {
            if !title1.isEqual(nil) {
                elements.setObject(title1, forKey: "title" as NSCopying)
            }
            if !date.isEqual(nil) {
                elements.setObject(date, forKey: "date" as NSCopying)
            }
            if !imageURL.isEqual(nil) {
                elements.setObject(imageURL, forKey: "imageurl" as NSCopying)
            }
            post.add(elements)
        }
    }
    
    /* 테이블 뷰 함수 */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return post.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "KoreaCell", for: indexPath)
        cell.textLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "title") as! NSString as String
        cell.detailTextLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "date") as! NSString as String
        
        // ## 동작 안함. url에 대한 접근이 이뤄지지 않음. 크롬에서 확인했음.
        if let url = URL(string: (post.object(at: indexPath.row) as AnyObject).value(forKey: "imageurl") as! NSString as String) {
            print((post.object(at: indexPath.row) as AnyObject).value(forKey: "imageurl") as! NSString as String)
            if let data = try? Data(contentsOf: url) {
                cell.imageView?.image = UIImage(data: data)
            }
        }
        return cell
    }

}

