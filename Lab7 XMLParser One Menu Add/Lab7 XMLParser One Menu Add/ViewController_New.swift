//
//  File.swift
//  XMLParsingDemo
//
//  Created by kpugame on 2020/05/26.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController_New: UIViewController, XMLParserDelegate , UITableViewDataSource {
    
    /* outlets */
    @IBOutlet weak var tableData: UITableView!
    
    /* XML 관련 변수 */
    //  xml file을 download, parse하는 오브젝트
    var parser = XMLParser()
    //  feed 데이터를 저장하는 mutable array
    var post = NSMutableArray()
    //  title, date 같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()
    //  저장할 데이터 변수
    var title1 = NSMutableString()
    var date = NSMutableString()
    var imageURL = NSMutableString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
    }
    
    /* 파싱 함수 */
    func beginParsing() {
        post = []
        parser = XMLParser(contentsOf:(URL(string:"https://openapi.gg.go.kr/RegionMnyFacltStus?KEY=4f7070fb285a4bd4b65fa9f040440f17"))!)!
        parser.delegate = self
        parser.parse()
        tableData!.reloadData()
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        /* parser가 새로운 element를 발견하면 변수를 생성한다. */
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "row") {
            elements = NSMutableDictionary()
            elements = [:]
            title1 = NSMutableString()
            title1 = ""
            date = NSMutableString()
            date = ""
            imageURL = NSMutableString()
            imageURL = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        /* title & pubDate을 발견하면 title1과 date에 완성한다. */
        if element.isEqual(to: "CMPNM_NM") {
            title1.append(string)
        } else if element.isEqual(to: "REFINE_ROADNM_ADDR") {
            date.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        /* element의 끄테서 feed데이터를 dictionary에 저장 */
        if (elementName as NSString).isEqual(to: "row") {
            if !title1.isEqual(nil) {
                elements.setObject(title1, forKey: "CMPNM_NM" as NSCopying)
            }
            if !date.isEqual(nil) {
                elements.setObject(date, forKey: "REFINE_ROADNM_ADDR" as NSCopying)
            }
            post.add(elements)
        }
    }
    
    /* 테이블 뷰 함수 */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return post.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewXMLCell", for: indexPath)
        cell.textLabel?.text = "상호명:" + ((post.object(at: indexPath.row) as AnyObject).value(forKey: "CMPNM_NM") as! NSString as String)
        cell.detailTextLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "REFINE_ROADNM_ADDR") as! NSString as String
        
        return cell
    }
}
