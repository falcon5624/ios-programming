//
//  ViewController.swift
//  XMLParsingDemo
//
//  Created by kpugame on 2020/05/19.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController_Naver : UIViewController, XMLParserDelegate , UITableViewDataSource {
    
    /* outlets */
    @IBOutlet weak var tableData: UITableView!
    
    /* XML 관련 변수 */
    //  xml file을 download, parse하는 오브젝트
    var parser = XMLParser()
    //  feed 데이터를 저장하는 mutable array
    var post = NSMutableArray()
    //  title, date 같은 feed 데이터를 저장하는 mutable dictionary
    var elements = NSMutableDictionary()
    var element = NSString()
    //  저장 문자열 변수 
    var title1 = NSMutableString()
    var date = NSMutableString()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        beginParsing()
    }
    
    /* 파싱 함수 */
    func beginParsing() {
        post = []
        // naver openAPI 책 검색 "love" 키워드로
        // naver rest api 는 http header에 x-naver-client-id와 x-naver-client-secret을 추가해야함.
        //  따라서 urlRequest 를 구성한다.
        let client_id = "J0xlzLY_mwqXVGY7OBho"
        let client_secret = "8NphEmVq6H"
        let naverURL = URL(string: "https://openapi.naver.com/v1/search/book.xml?query=love&dispaly=10&start=1")!
        var naverURLRequest = URLRequest(url: naverURL)
        naverURLRequest.addValue(client_id, forHTTPHeaderField: "X-Naver-Client-Id")
        naverURLRequest.addValue(client_secret, forHTTPHeaderField: "X-Naver-Client-Secret")
        
        //URLSession으로 xml데이터 가져오고 xmlParser호출
        URLSession.shared.dataTask(with : naverURLRequest) {
            data, response, error in if let data = data {
                self.parser = XMLParser(data:data)
                self.parser.delegate = self
                self.parser.parse()
                //TableView.reloadData() 는 main thread에서 호출되어야 하기 때문에 dispatchQueue로 보내서 실행
                DispatchQueue.main.async {
                    self.tableData!.reloadData()
                }
            }
            }.resume()
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        /* parser가 새로운 element를 발견하면 변수를 생성한다. */
        element = elementName as NSString
        if (elementName as NSString).isEqual(to: "item") {
            elements = NSMutableDictionary()
            elements = [:]
            title1 = NSMutableString()
            title1 = ""
            date = NSMutableString()
            date = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        /* title & pubDate을 발견하면 title1과 date에 완성한다. */
        if element.isEqual(to: "title") {
            title1.append(string)
        } else if element.isEqual(to: "author") {
            date.append(string)
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        /* element의 끄테서 feed데이터를 dictionary에 저장 */
        if (elementName as NSString).isEqual(to: "item") {
            if !title1.isEqual(nil) {
                elements.setObject(title1, forKey: "title" as NSCopying)
            }
            if !date.isEqual(nil) {
                elements.setObject(date, forKey: "date" as NSCopying)
            }
            post.add(elements)
        }
    }
    
    /* 테이블 뷰 함수 */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return post.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "title") as! NSString as String
        cell.detailTextLabel?.text = (post.object(at: indexPath.row) as AnyObject).value(forKey: "date") as! NSString as String
        return cell
    }

}

