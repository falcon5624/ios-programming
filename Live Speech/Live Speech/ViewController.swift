//
//  ViewController.swift
//  Live Speech
//
//  Created by kpugame on 2020/05/23.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit
import Speech

class ViewController: UIViewController {

    /* outlets */
    @IBOutlet weak var startListenButton: UIButton! //  transcribe button
    @IBOutlet weak var stopListenButton: UIButton!  //  stop transcribe
    @IBOutlet weak var textView: UITextView!        //  text view
    
    /* action func */
    @IBAction func startTranscribing(_ sender: Any) {
        startListenButton.isEnabled = false
        stopListenButton.isEnabled = true
        try! startSession()
    }
    @IBAction func stopTransribing(_ sender: Any) {
        if audioEngine.isRunning {
            audioEngine.stop()
            speechRecognitionRequest?.endAudio()
            startListenButton.isEnabled = true
            stopListenButton.isEnabled = false
        }
    }
    
    func startSession() throws {
        if let recognitionTask = speechRecognitionTask {
            recognitionTask.cancel()
            self.speechRecognitionTask = nil
        }
        let audioSession = AVAudioSession.sharedInstance()
        try audioSession.setCategory(AVAudioSession.Category.record)
        
        speechRecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        guard let recognitionRequest = speechRecognitionRequest else {
            fatalError("SFSpeechAudioBufferRecognitionRequest object creation failed")
        }
        
        let inputNode = audioEngine.inputNode
        recognitionRequest.shouldReportPartialResults = true
        speechRecognitionTask = speechRecognizer.recognitionTask(with: recognitionRequest) {
            result, error in
            var finished = false
            if let result = result {
                self.textView.text = result.bestTranscription.formattedString
                finished = result.isFinal
            }
            if error != nil || finished {
                self.audioEngine.stop()
                inputNode.removeTap(onBus: 0)
                
                self.speechRecognitionRequest = nil
                self.speechRecognitionTask = nil
                self.startListenButton.isEnabled = true
            }
        }
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) {
            (buffer : AVAudioPCMBuffer, when : AVAudioTime) in
            self.speechRecognitionRequest?.append(buffer)
        }
        audioEngine.prepare()
        try audioEngine.start()
    }
    
    /* 음성인식 객체 */
    private let speechRecognizer = SFSpeechRecognizer(locale: Locale(identifier: "ko-KR"))!
    private var speechRecognitionRequest : SFSpeechAudioBufferRecognitionRequest?
    private var speechRecognitionTask : SFSpeechRecognitionTask?
    private let audioEngine = AVAudioEngine()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authorizeSR()
    }

    func authorizeSR() {
        SFSpeechRecognizer.requestAuthorization {
            authStatus in OperationQueue.main.addOperation {
                switch authStatus {
                case .authorized:
                    self.startListenButton.isEnabled = true
                    break
                case .denied:
                    self.startListenButton.isEnabled = false
                    self.startListenButton.setTitle("Speech recognition access denied by user", for: .disabled)
                    break
                case .restricted:
                    self.startListenButton.isEnabled = false
                    self.startListenButton.setTitle("Speech recognition restricted on device", for: .disabled)
                    break
                case .notDetermined:
                    self.startListenButton.isEnabled = false
                    self.startListenButton.setTitle("Speech recognition not authorized", for: .disabled)
                    break
                }
            }
        }
    }

}

