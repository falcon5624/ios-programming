//
//  Player.swift
//  Lab5 Ratings Star
//
//  Created by kpugame on 2020/04/17.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

enum Games : String {
    /* 게임 리스트 */
    case worldOfWarcraft = "월드 오브 워크래프트"
    case leagueOfLegend = "리그 오브 레전드"
    case hearthStone = "하스스톤"
    case warZone = "콜 오브 듀티 : 워존"
    case diablo4 = "디아블로 4"
    case animalCrossing = "모여봐요 동물의 숲"
}

enum Ratings : Int {
    /* 별점 리스트 */
    case one = 1
    case two = 2
    case three = 3
    case four = 4
    case five = 5
}

class Player {
    var name : String?          //  플레이어 이름
    var game : Games            //  플레이어가 선호하는 게임
    var rating : Ratings        //  해당 평점
    
    init (name : String?, game : Games, rating : Ratings) {
        self.name = name
        self.game = game
        self.rating = rating
    }
}

//  플레이어 상수 초기 데이터
let a_players : [Player] = [
    Player(name: "송민수", game: .worldOfWarcraft, rating: .five),
    Player(name: "김선균", game: .hearthStone, rating: .three),
    Player(name: "김홍남", game: .warZone, rating: .four),
]

