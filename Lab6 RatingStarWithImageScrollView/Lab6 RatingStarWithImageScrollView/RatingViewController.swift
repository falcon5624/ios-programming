//
//  RatingViewController.swift
//  Lab5 Ratings Star
//
//  Created by kpugame on 2020/04/20.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class RatingViewController: UITableViewController {

    let m_aRatingScores : [Ratings] = [
        .one,.two, .three, .four, .five
    ]
    var m_nSelectedRatingIndex : Int?
    var m_selectedRating : Ratings! {
        didSet {
            if let rating = m_selectedRating {
                m_nSelectedRatingIndex = m_aRatingScores.firstIndex(of: rating)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SaveSelectedRating" {
            if let cell = sender as? UITableViewCell {
                let indexPath = tableView.indexPath(for: cell)
                if let index = indexPath?.row {
                    m_selectedRating = m_aRatingScores[index]
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return m_aRatingScores.count
    }

    //  RatingCell의 태그는 1000
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RatingCell", for: indexPath)
        let rating : Ratings = m_aRatingScores[indexPath.row]
        
        if let imageView = cell.viewWithTag(1000) as? UIImageView {
            imageView.image = UIImage(named: "\(rating.rawValue)Stars")
        }
        if indexPath.row == m_nSelectedRatingIndex {
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if let index = m_nSelectedRatingIndex {
            let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
            cell?.accessoryType = .none
        }
        
        m_selectedRating = m_aRatingScores[indexPath.row]
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
    }
    
}
