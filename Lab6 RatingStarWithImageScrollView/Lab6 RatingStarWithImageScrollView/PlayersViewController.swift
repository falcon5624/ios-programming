//
//  PlayersViewController.swift
//  Lab5 Ratings Star
//
//  Created by kpugame on 2020/04/18.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class PlayersViewController: UITableViewController {
    
    //  플레이어 배열
    var m_aplayers : [Player] = a_players
    
    @IBAction func UnwindCancelToPlayerView(segue : UIStoryboardSegue) {
           /* Cancel Unwind 메서드 */
           
    }
    
    @IBAction func UnwindSaveToPlayerView(segue : UIStoryboardSegue) {
           /* Save Unwind 메서드 */
        if let playerDetailViewController = segue.source as? PlayerDetailViewController {
            if let player = playerDetailViewController.m_newPlayer {
                m_aplayers.append(player)
                
                let indexPath = IndexPath(row: m_aplayers.count - 1, section: 0)
                tableView.insertRows(at: [indexPath], with: .automatic)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        //  return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //  return the number of rows
        return m_aplayers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerCell", for: indexPath) as! PlayerCell
        let player : Player = m_aplayers[indexPath.row] as Player
        cell.m_player = player
        return cell
    }
}
