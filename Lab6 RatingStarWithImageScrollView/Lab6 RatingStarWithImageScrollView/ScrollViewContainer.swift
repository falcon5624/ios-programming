//
//  ScrollViewContainer.swift
//  ScrollViews
//
//  Created by kpugame on 2020/05/04.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ScrollViewContainer: UIView {
    //  페이지 바깥을 눌러도 페이지가 변하도록 넓은 scrollView를 만든다.
    @IBOutlet var scrollView : UIScrollView!
    
    //  스크롤뷰 컨테이너 바운드 안을 누르기만 하면 스크롤 뷰를 리턴한다.
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)
        if let theView = view {
            if theView == self {
                return scrollView
            }
        }
        return view
    }
}
