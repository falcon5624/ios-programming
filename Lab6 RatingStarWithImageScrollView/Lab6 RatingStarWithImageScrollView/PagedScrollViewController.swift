//
//  PagedScrollViewController.swift
//  ScrollViews
//
//  Created by kpugame on 2020/04/22.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class PagedScrollViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet var scrollView : UIScrollView!
    @IBOutlet var pageControl : UIPageControl!
    
    //  1페이지마다 1개의 이미지를 가지는 UIImage 배열 선언
    var m_pageImages : [UIImage] = []
    //  UIImageView 배열; optional이다. 필요할 때 로딩하기 때문에.
    var m_pageViews : [UIImageView?] = []
    
    override func viewDidLoad() {
        
        //  make UIImages from 5 photos
        m_pageImages = [
        UIImage(named: "photo1.png")!,
        UIImage(named: "photo2.png")!,
        UIImage(named: "photo3.png")!,
        UIImage(named: "photo4.png")!,
        UIImage(named: "photo5.png")!
        ]
        
        let pageCount = m_pageImages.count
        
        //  current page index is 0, set pageControl's page count
        pageControl.currentPage = 0
        pageControl.numberOfPages = pageCount
        
        //  make pageView array's element as nil, 페이지 갯수만큼 만든다.
        //  나중에 로딩할 때 pageImages에서 가져온다.
        for _ in 0..<pageCount {
            m_pageViews.append(nil)
        }
        
        //  스크롤 뷰 컨텐트 width = 스크롤 뷰 프레임 width x pageImages 개수
        let pagesScrollViewSize = scrollView.frame.size
        scrollView.contentSize = CGSize(width: pagesScrollViewSize.width * CGFloat(m_pageImages.count), height: pagesScrollViewSize.height)
        
        //  image loading
        LoadVisiblePages()
    }
    
    func LoadPage(_ page : Int) {
        if page < 0 || page >= m_pageImages.count {
            //  페이지 인덱스 범위를 벗어나면 리턴
            return
        }
        
        //  초기에 pageViews 배열은 nil로 이루어져 있는데, nil이 아니라면 로딩이 된 것이므로 아무런 할 일이 없다.
        if m_pageViews[page]  != nil {
            
        } else {
            //  pageView가 nil이면 로딩해야 한다.
            //  y offset은 0, x offset은 화면너비 x 페이지 인덱스로 설정
            var frame = scrollView.bounds
            frame.origin.x = frame.size.width * CGFloat(page)
            frame.origin.y = 0.0
            
            //  새로운 UIImageView를 생성하고 scrollView에 추가한다.
            let newPageView = UIImageView(image: m_pageImages[page])
            newPageView.contentMode = .scaleAspectFit
            newPageView.frame = frame
            scrollView.addSubview(newPageView)
            
            //  새로 생성한 newPageView를 배열의 nil 대신에 삽입한다.
            m_pageViews[page] = newPageView
        }
    }
    
    func PurgePage(_ page : Int) {
        /* 현재 페이지 앞뒤를 제외하고 나머지는 pageViews 배열에서 제거한다. */
        if page < 0 || page >= m_pageImages.count {
            //  페이지 인덱스를 벗어나면 리턴
            return
        }
        
        //  pageViews[page]가 nil이 아니라면 scrollView에서 제거하고
        //  pageViews[page]는 nil로 만듦
        if let pageView = m_pageViews[page] {
            pageView.removeFromSuperview()
            m_pageViews[page] = nil
        }
    }
    
    /* 현재 페이지와 그 앞 뒤 뺴곤 다 purge 시킨다. */
    func LoadVisiblePages() {
        //  count current page number
        let pageWidth = scrollView.frame.width
        let page = Int(floor((scrollView.contentOffset.x * 2.0 + pageWidth) / (pageWidth * 2.0)))
        
        //  pageControl 현재 페이지 갱신
        pageControl.currentPage = page
        
        //  현재 페이지 앞뒤 페이지 설정
        let firstPage = page - 1
        let lastPage = page + 1
        
        //  페이지 0..<앞페이지 전까지 제거
        for index in 0..<firstPage + 1 {
            PurgePage(index)
        }
        
        //  앞, 현재, 뒤 3개 페이지만 로딩
        for index in firstPage ... lastPage {
            LoadPage(index)
        }
        
        //  뒤페이지<..끝페이지까지 제거
        for index in lastPage + 1 ..< m_pageImages.count + 1 {
            PurgePage(index)
        }
    }
    
    //  UIScrollViewDelegage method
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //  Load the pages that are now on screen
        LoadVisiblePages()
    }
}
