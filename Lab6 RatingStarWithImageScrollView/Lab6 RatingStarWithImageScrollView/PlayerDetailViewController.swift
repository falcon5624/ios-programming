//
//  PlayerDetailViewController.swift
//  Lab5 Ratings Star
//
//  Created by kpugame on 2020/04/18.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

private enum SectionNumber : Int {
    case playerName = 0
    case game = 1
    case ratings = 2
}

class PlayerDetailViewController: UITableViewController {
    
    /* Outlet 변수들 */
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var gameLabelSelected: UILabel!
    @IBOutlet weak var ratingImageView: UIImageView!
    
    var m_newPlayer : Player!
    var m_game : Games! = .hearthStone {
        didSet {
            gameLabelSelected.text? = m_game.rawValue
        }
    }
    var m_rating : Ratings! = .five {
        didSet {
            ratingImageView.image = UIImage(named: "\(m_rating.rawValue)Stars")
        }
    }
    
    /* unwind 함수 */
    @IBAction func UnwindWithSelectedRating(segue : UIStoryboardSegue) {
        if let ratingViewController = segue.source as? RatingViewController, let selectedRating = ratingViewController.m_selectedRating {
            m_rating = selectedRating
        }
    }
    
    @IBAction func UnwindWithSelectedGame(segue : UIStoryboardSegue) {
        if let gameViewController = segue.source as? GameViewController, let selectedGame = gameViewController.m_selectedGame {
            m_game = selectedGame
        }
    }
    
    /* prepare 함수 오버라이드 */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SaveToPlayerView" {
            m_newPlayer = Player(name: nameTextField.text, game: m_game, rating: m_rating)
        }
        
        if segue.identifier == "PickGame" {
            if let gameViewController = segue.destination as? GameViewController {
                gameViewController.m_selectedGame = m_game
            }
        }
        
        if segue.identifier == "PickRating" {
            if let ratingViewController = segue.destination as? RatingViewController {
                ratingViewController.m_selectedRating = m_rating
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //  PlayerName Section의 NameTextField의 어디를 탭해도 선택되도록 한다.
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == SectionNumber.playerName.rawValue {
            nameTextField.becomeFirstResponder()
        }
    }
}
