//
//  ViewController.swift
//  Tap Me
//
//  Created by kpugame on 2020/03/27.
//  Copyright © 2020 kpugame. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    /*
     IB는 interface builder, outlet은 연결의 의미를 가진다.
     왜 !를 써주게 되냐면, 라벨 연결을 반드시 성공할 것이다는 것. 라이브러리가 없다면 nil이 되어
     프로그램이 제대로 동작하지 않을 것이니까
     */
    
    @IBOutlet var scoreLabel : UILabel!
    
    @IBOutlet var timerLabel : UILabel!

    var score : Int = 0             //  터치할 때마다 점수 1을 획득
    var seconds : Int = 0           //  30초의 제한 시간을 가진다.
    var timer = Timer()             //  Timer() 인스턴스 생성
    var isStarted : Bool = false    //  초기 동작인지의 부울린 변수
    
    @IBAction func buttonPressed() {
        //  버튼이 눌렸을 때 처리하는 함수
        
        if !isStarted {isStarted = true; Setup()}
        //  처음 눌렸다면
        
        score += 1
        scoreLabel.text = "Score:\(score)"
    }
    
    func Setup() {
        seconds = 30
        score = 0
        timerLabel.text = "Timer:\(seconds)"
        scoreLabel.text = "Score:\(score)"
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.SubtractTime), userInfo: nil, repeats: true)
    }
    
    @objc func SubtractTime() {
        seconds -= 1
        timerLabel.text = "Timer:\(seconds)"
        
        if (seconds == 0) {
            timer.invalidate()
            let alert = UIAlertController(title : "Time's Up", message : "You got \(score) points!", preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "Play Again", style: UIAlertAction.Style.default, handler: { action in self.Setup()}))
            
            present(alert ,animated: true, completion : nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        scoreLabel.text = "Ready.."
    }


}

