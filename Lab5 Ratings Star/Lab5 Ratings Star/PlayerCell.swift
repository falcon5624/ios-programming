//
//  PlayerCell.swift
//  Lab5 Ratings Star
//
//  Created by kpugame on 2020/04/18.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class PlayerCell: UITableViewCell {

    @IBOutlet weak var ratingStarsImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var gameLabel: UILabel!
    
    var m_player : Player! {
        didSet {
            ratingStarsImageView.image =  UIImage(named: "\(m_player.rating.rawValue)Stars")
            nameLabel.text = m_player.name
            gameLabel.text = m_player.game.rawValue
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
