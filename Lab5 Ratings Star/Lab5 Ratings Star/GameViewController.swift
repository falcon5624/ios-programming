//
//  GameViewController.swift
//  Lab5 Ratings Star
//
//  Created by kpugame on 2020/04/19.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class GameViewController: UITableViewController {

    /* 일반 변수 선언 */
    //  게임 리스트
    var m_aGames : [Games] = [
    .hearthStone,
    .leagueOfLegend,
    .warZone,
    .worldOfWarcraft,
    .diablo4,
    .animalCrossing
    ]
    
    var m_nSelectedGameIndex : Int?
    var m_selectedGame : Games! {
        didSet {
            if let game = m_selectedGame {
                m_nSelectedGameIndex = m_aGames.firstIndex(of: game)!
            }
        }
    }
    
    /*  row가 선택되면 selectedGame을 업데이트하는 tableView 메서드가 실행되기 전에
        unwindSegue가 먼저 실행되므로 selectedGame을 unwind 이전에 업데이트 해야 함.
     */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SaveSelectedGame" {
            if let cell = sender as? UITableViewCell {
                let indexPath = tableView.indexPath(for: cell)
                if let index = indexPath?.row {
                    m_selectedGame = m_aGames[index]
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return m_aGames.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GameCell", for: indexPath)
        let game = m_aGames[indexPath.row]
        cell.textLabel?.text = game.rawValue
        
        //  현재 row가 선택된 게임이라면 checkmark를 추가한다.
        if indexPath.row == m_nSelectedGameIndex{
            cell.accessoryType = .checkmark
        } else {
            cell.accessoryType = .none
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //  cell 선택 애니메이션 효과 ; 회색 그라데이션 처리
        tableView.deselectRow(at: indexPath, animated: true)
        
        //  이전 선택된 row의 checkmark 표시 해제
        if let index = m_nSelectedGameIndex {
            let cell = tableView.cellForRow(at: IndexPath(row: index, section: 0))
            cell?.accessoryType = .none
        }
        //  row 선택하면 그 row에 해당되는 게임을 selectedGame에 저장
        m_selectedGame = m_aGames[indexPath.row]
        
        //  현재 선택된 cell을 checkmark 표시
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .checkmark
    }
}
