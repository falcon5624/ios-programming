//
//  ViewController.swift
//  Lab10 Yahzee
//
//  Created by kpugame on 2020/06/02.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    /* outlets */
    @IBOutlet weak var outletRollDice: UIButton!
    @IBOutlet weak var outletDice1: UIButton!
    @IBOutlet weak var outletDice2: UIButton!
    @IBOutlet weak var outletDice3: UIButton!
    @IBOutlet weak var outletDice4: UIButton!
    @IBOutlet weak var outletDice5: UIButton!
    @IBAction func actionRollDice(_ sender: Any) {
        /* Sound Effects */
        audioController.playerEffect(name: SoundDing)
        
        if outletRollDice.isEnabled {
            rollDice()
            switch m_roll {
            case 0,1:
                m_roll += 1
                outletRollDice.setTitle("Roll Again", for: UIControl.State.normal)
                outletGameMessage.text = "간직할 주사위를 선택한 후 Roll Again을 누르세요"
                break
            case 2:
                m_roll = 0
                outletGameMessage.text = "이제 남아있는 Category를 선택하세요"
                outletRollDice.isEnabled = false
                m_round += 1
                break
            default:
                break
            }
        }
    }
    @IBAction func actionDice1(_ sender: Any) {
        if m_roll != 0 {
            outletDice1.backgroundColor = UIColor.gray
            outletDice1.isEnabled = false
            
            UIView.animate(
                withDuration: 0.5,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice1.center = self.dicePos[0]
                    self.outletDice1.transform = CGAffineTransform(rotationAngle: 0)
            },
                completion: nil)
            audioController.playerEffect(name: SoundWrong)
        }
    }
    @IBAction func actionDice2(_ sender: Any) {
        if m_roll != 0 {
            outletDice2.backgroundColor = UIColor.gray
            outletDice2.isEnabled = false
            
            UIView.animate(
                withDuration: 0.5,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice2.center = self.dicePos[1]
                    self.outletDice2.transform = CGAffineTransform(rotationAngle: 0)
            },
                completion: nil)
            audioController.playerEffect(name: SoundWrong)
        }
    }
    @IBAction func actionDice3(_ sender: Any) {
        if m_roll != 0 {
            outletDice3.backgroundColor = UIColor.gray
            outletDice3.isEnabled = false
            
            UIView.animate(
                withDuration: 0.5,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice3.center = self.dicePos[2]
                    self.outletDice3.transform = CGAffineTransform(rotationAngle: 0)
            },
                completion: nil)
            audioController.playerEffect(name: SoundWrong)
        }
    }
    @IBAction func actionDice4(_ sender: Any) {
        if m_roll != 0 {
            outletDice4.backgroundColor = UIColor.gray
            outletDice4.isEnabled = false
            
            UIView.animate(
                withDuration: 0.5,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice4.center = self.dicePos[3]
                    self.outletDice4.transform = CGAffineTransform(rotationAngle: 0)
            },
                completion: nil)
            audioController.playerEffect(name: SoundWrong)
        }
    }
    @IBAction func actionDice5(_ sender: Any) {
        if m_roll != 0 {
            outletDice5.backgroundColor = UIColor.gray
            outletDice5.isEnabled = false
            
            UIView.animate(
                withDuration: 0.5,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice5.center = self.dicePos[4]
                    self.outletDice5.transform = CGAffineTransform(rotationAngle: 0)
            },
                completion: nil)
            audioController.playerEffect(name: SoundWrong)
        }
    }
    
    @IBOutlet weak var outletCategory1: UIButton!
    @IBOutlet weak var outletCategory2: UIButton!
    @IBOutlet weak var outletCategory3: UIButton!
    @IBOutlet weak var outletCategory4: UIButton!
    @IBOutlet weak var outletCategory5: UIButton!
    @IBOutlet weak var outletCategory6: UIButton!
    @IBOutlet weak var outletCategory7: UIButton!
    @IBOutlet weak var outletCategory8: UIButton!
    @IBOutlet weak var outletCategory9: UIButton!
    @IBOutlet weak var outletUpperTotal: UIButton!
    @IBOutlet weak var outletLowerTotal: UIButton!
    @IBOutlet weak var outletGrandTotal: UIButton!
    @IBOutlet weak var outletUpperBonus: UIButton!
    @IBOutlet weak var outletCategory10: UIButton!
    @IBOutlet weak var outletCategory11: UIButton!
    @IBOutlet weak var outletCategory12: UIButton!
    @IBOutlet weak var outletCategory13: UIButton!
    @IBAction func actionCategory1(_ sender: Any) {
        if outletCategory1.isEnabled {
            m_scores[0] = upperScore(index: 1)
            m_isUsed[0] = true
            outletCategory1.setTitle(String(m_scores[0]), for: UIControl.State.normal)
            outletCategory1.isEnabled = false
            outletCategory1.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory1.imageView?.center.x)!, y: (outletCategory1.imageView?.center.y)!, width: 10, height: 10))
            outletCategory1.imageView?.superview?.addSubview(explore)
            outletCategory1.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory2(_ sender: Any) {
        if outletCategory2.isEnabled {
            m_scores[1] = upperScore(index: 2)
            m_isUsed[1] = true
            outletCategory2.setTitle(String(m_scores[1]), for: UIControl.State.normal)
            outletCategory2.isEnabled = false
            outletCategory2.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory2.imageView?.center.x)!, y: (outletCategory2.imageView?.center.y)!, width: 10, height: 10))
            outletCategory2.imageView?.superview?.addSubview(explore)
            outletCategory2.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory3(_ sender: Any) {
        if outletCategory3.isEnabled {
            m_scores[2] = upperScore(index: 3)
            m_isUsed[2] = true
            outletCategory3.setTitle(String(m_scores[2]), for: UIControl.State.normal)
            outletCategory3.isEnabled = false
            outletCategory3.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory3.imageView?.center.x)!, y: (outletCategory3.imageView?.center.y)!, width: 10, height: 10))
            outletCategory3.imageView?.superview?.addSubview(explore)
            outletCategory3.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory4(_ sender: Any) {
        if outletCategory4.isEnabled {
            m_scores[3] = upperScore(index: 4)
            m_isUsed[3] = true
            outletCategory4.setTitle(String(m_scores[3]), for: UIControl.State.normal)
            outletCategory4.isEnabled = false
            outletCategory4.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory4.imageView?.center.x)!, y: (outletCategory4.imageView?.center.y)!, width: 10, height: 10))
            outletCategory4.imageView?.superview?.addSubview(explore)
            outletCategory4.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory5(_ sender: Any) {
        if outletCategory5.isEnabled {
            m_scores[4] = upperScore(index: 5)
            m_isUsed[4] = true
            outletCategory5.setTitle(String(m_scores[4]), for: UIControl.State.normal)
            outletCategory5.isEnabled = false
            outletCategory5.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory5.imageView?.center.x)!, y: (outletCategory5.imageView?.center.y)!, width: 10, height: 10))
            outletCategory5.imageView?.superview?.addSubview(explore)
            outletCategory5.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory6(_ sender: Any) {
        if outletCategory6.isEnabled {
            m_scores[5] = upperScore(index: 6)
            m_isUsed[5] = true
            outletCategory6.setTitle(String(m_scores[5]), for: UIControl.State.normal)
            outletCategory6.isEnabled = false
            outletCategory6.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory6.imageView?.center.x)!, y: (outletCategory6.imageView?.center.y)!, width: 10, height: 10))
            outletCategory6.imageView?.superview?.addSubview(explore)
            outletCategory6.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory7(_ sender: Any) {
        // 트리플
        if outletCategory7.isEnabled {
            m_scores[6] = scoreThreeOfAKind()
            m_isUsed[6] = true
            outletCategory7.setTitle(String(m_scores[6]), for: UIControl.State.normal)
            outletCategory7.isEnabled = false
            outletCategory7.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory7.imageView?.center.x)!, y: (outletCategory7.imageView?.center.y)!, width: 10, height: 10))
            outletCategory7.imageView?.superview?.addSubview(explore)
            outletCategory7.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory8(_ sender: Any) {
        // 쿼드라
        if outletCategory8.isEnabled {
            m_scores[7] = scoreFourOfAKind()
            m_isUsed[7] = true
            outletCategory8.setTitle(String(m_scores[7]), for: UIControl.State.normal)
            outletCategory8.isEnabled = false
            outletCategory8.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory8.imageView?.center.x)!, y: (outletCategory8.imageView?.center.y)!, width: 10, height: 10))
            outletCategory8.imageView?.superview?.addSubview(explore)
            outletCategory8.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory9(_ sender: Any) {
        /* 풀하우스 */
        if outletCategory9.isEnabled {
            m_scores[8] = fullhouse()
            m_isUsed[8] = true
            outletCategory9.setTitle(String(m_scores[8]), for: UIControl.State.normal)
            outletCategory9.isEnabled = false
            outletCategory9.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory9.imageView?.center.x)!, y: (outletCategory9.imageView?.center.y)!, width: 10, height: 10))
            outletCategory9.imageView?.superview?.addSubview(explore)
            outletCategory9.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory10(_ sender: Any) {
        /* 스몰 스트레이트 */
        if outletCategory10.isEnabled {
            m_scores[9] = smallStraight()
            m_isUsed[9] = true
            outletCategory10.setTitle(String(m_scores[9]), for: UIControl.State.normal)
            outletCategory10.isEnabled = false
            outletCategory10.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory10.imageView?.center.x)!, y: (outletCategory10.imageView?.center.y)!, width: 10, height: 10))
            outletCategory10.imageView?.superview?.addSubview(explore)
            outletCategory10.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory11(_ sender: Any) {
        /* 라지 스트레이트 */
        if outletCategory11.isEnabled {
            m_scores[10] = largeStraight()
            m_isUsed[10] = true
            outletCategory11.setTitle(String(m_scores[10]), for: UIControl.State.normal)
            outletCategory11.isEnabled = false
            outletCategory11.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory11.imageView?.center.x)!, y: (outletCategory11.imageView?.center.y)!, width: 10, height: 10))
            outletCategory11.imageView?.superview?.addSubview(explore)
            outletCategory11.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory12(_ sender: Any) {
        /* 야찌 */
        if outletCategory12.isEnabled {
            m_scores[11] = yahtzee()
            m_isUsed[11] = true
            outletCategory12.setTitle(String(m_scores[11]), for: UIControl.State.normal)
            outletCategory12.isEnabled = false
            outletCategory12.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory12.imageView?.center.x)!, y: (outletCategory12.imageView?.center.y)!, width: 10, height: 10))
            outletCategory12.imageView?.superview?.addSubview(explore)
            outletCategory12.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBAction func actionCategory13(_ sender: Any) {
        /* 찬스 */
        if outletCategory13.isEnabled {
            m_scores[12] = sumDice()
            m_isUsed[12] = true
            outletCategory13.setTitle(String(m_scores[12]), for: UIControl.State.normal)
            outletCategory13.isEnabled = false
            outletCategory13.backgroundColor = UIColor.gray
            
            /* particles */
            let explore = ExplodeView(frame: CGRect(x: (outletCategory13.imageView?.center.x)!, y: (outletCategory13.imageView?.center.y)!, width: 10, height: 10))
            outletCategory13.imageView?.superview?.addSubview(explore)
            outletCategory13.imageView?.superview?.sendSubviewToBack(_: explore)
            audioController.playerEffect(name: SoundDing)
            
            toDoAfterCategory()
        }
    }
    @IBOutlet weak var outletGameMessage: UILabel!
    
    /* view properties */
    var m_roll : Int = 0  // 한 라운드 내에서의 주사위 던진 횟수
    var m_round : Int = 0 // 총 13 라운드
    var m_dices : [Int] = [1, 1, 1, 1, 1]
    var m_scores : [Int] = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,]
    var m_isUsed : [Bool] = [false, false, false, false, false, false, false, false, false, false, false, false, false]
    var dicePos : [CGPoint] = [
        CGPoint(x: 0, y: 0),
        CGPoint(x: 0, y: 0),
        CGPoint(x: 0, y: 0),
        CGPoint(x: 0, y: 0),
        CGPoint(x: 0, y: 0)
    ]
    var audioController : AudioController
    required init?(coder: NSCoder) {
        audioController = AudioController()
        audioController.preloadAudioEffects(audioFileNames: AudioEffectFiles)
        super.init(coder : coder)
    }
    
    func rollDice() {
        if outletDice1.isEnabled {
            m_dices[0] = Int((arc4random() % 6) + 1)
            outletDice1.setTitle(String(m_dices[0]), for: UIControl.State.normal)
            
            // dice animation
            UIView.animate(
                withDuration: Double(randomNumber(minX: 1, maxX: 5)) / 10.0,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice1.center = CGPoint(x: randomNumber(minX: 200, maxX: 600),
                                                      y: randomNumber(minX: 100, maxX: 150))
                    self.outletDice1.transform = CGAffineTransform(rotationAngle: CGFloat(randomNumber(minX: 0, maxX: 20)))
            },
                completion: nil)
        }
        if outletDice2.isEnabled {
            m_dices[1] = Int((arc4random() % 6) + 1)
            outletDice2.setTitle(String(m_dices[1]), for: UIControl.State.normal)
            
            // dice animation
            UIView.animate(
                withDuration: Double(randomNumber(minX: 1, maxX: 5)) / 10.0,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice2.center = CGPoint(x: randomNumber(minX: 200, maxX: 600),
                                                      y: randomNumber(minX: 200, maxX: 250))
                    self.outletDice2.transform = CGAffineTransform(rotationAngle: CGFloat(randomNumber(minX: 0, maxX: 20)))
            },
                completion: nil)
        }
        if outletDice3.isEnabled {
            m_dices[2] = Int((arc4random() % 6) + 1)
            outletDice3.setTitle(String(m_dices[2]), for: UIControl.State.normal)
            
            // dice animation
            UIView.animate(
                withDuration: Double(randomNumber(minX: 1, maxX: 5)) / 10.0,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice3.center = CGPoint(x: randomNumber(minX: 200, maxX: 600),
                                                      y: randomNumber(minX: 300, maxX: 350))
                    self.outletDice3.transform = CGAffineTransform(rotationAngle: CGFloat(randomNumber(minX: 0, maxX: 20)))
            },
                completion: nil)
        }
        if outletDice4.isEnabled {
            m_dices[3] = Int((arc4random() % 6) + 1)
            outletDice4.setTitle(String(m_dices[3]), for: UIControl.State.normal)
            
            // dice animation
            UIView.animate(
                withDuration: Double(randomNumber(minX: 1, maxX: 5)) / 10.0,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice4.center = CGPoint(x: randomNumber(minX: 200, maxX: 600),
                                                      y: randomNumber(minX: 400, maxX: 450))
                    self.outletDice4.transform = CGAffineTransform(rotationAngle: CGFloat(randomNumber(minX: 0, maxX: 20)))
            },
                completion: nil)
        }
        if outletDice5.isEnabled {
            m_dices[4] = Int((arc4random() % 6) + 1)
            outletDice5.setTitle(String(m_dices[4]), for: UIControl.State.normal)
            
            // dice animation
            UIView.animate(
                withDuration: Double(randomNumber(minX: 1, maxX: 5)) / 10.0,
                delay: 0.0,
                options: UIView.AnimationOptions.curveEaseInOut,
                animations: {
                    self.outletDice5.center = CGPoint(x: randomNumber(minX: 200, maxX: 600),
                                                      y: randomNumber(minX: 500, maxX: 550))
                    self.outletDice5.transform = CGAffineTransform(rotationAngle: CGFloat(randomNumber(minX: 0, maxX: 20)))
            },
                completion: nil)
        }
        
    }
    
    func upperScore(index : Int) -> Int {
        var sum : Int = 0
        for i in 0..<5 {
            if m_dices[i] == index {
                sum += index
            }
        }
        return sum
    }
    func scoreThreeOfAKind() -> Int {
        var sameNum : Int
        for i in 0..<3 {
            sameNum = 0
            for j in i+1..<5 {
                if m_dices[i] == m_dices[j] {
                    sameNum += 1
                }
            }
            if sameNum >= 2 {
                return sumDice()
            }
        }
        return 0
    }
    func scoreFourOfAKind() -> Int {
        var sameNum : Int
        for i in 0..<3 {
            sameNum = 0
            for j in i+1..<5 {
                if m_dices[i] == m_dices[j] {
                    sameNum += 1
                }
            }
            if sameNum >= 3 {
                return sumDice()
            }
        }
        return 0
    }
    func yahtzee() -> Int {
        var sameNum : Int
        for i in 0..<3 {
            sameNum = 0
            for j in i+1..<5 {
                if m_dices[i] == m_dices[j] {
                    sameNum += 1
                }
            }
            if sameNum == 4 {
                return 50
            }
        }
        return 0
    }
    func fullhouse() -> Int {
        var twiceFlag = false
        var tripleFlag = false
        if countElement(value: 1) == 2 {
            twiceFlag = true
        }
        if countElement(value: 2) == 2 {
            twiceFlag = true
        }
        if countElement(value: 3) == 2 {
            twiceFlag = true
        }
        if countElement(value: 4) == 2 {
            twiceFlag = true
        }
        if countElement(value: 5) == 2 {
            twiceFlag = true
        }
        if countElement(value: 6) == 2 {
            twiceFlag = true
        }
        /* */
        if countElement(value: 1) == 3 {
            tripleFlag = true
        }
        if countElement(value: 2) == 3 {
            tripleFlag = true
        }
        if countElement(value: 3) == 3 {
            tripleFlag = true
        }
        if countElement(value: 4) == 3 {
            tripleFlag = true
        }
        if countElement(value: 5) == 3 {
            tripleFlag = true
        }
        if countElement(value: 6) == 3 {
            tripleFlag = true
        }
        
        if tripleFlag && twiceFlag {
            return 25
        }
        
        return 0
    }
    func countElement(value : Int) -> Int {
        var retValue : Int = 0
        for i in 0..<5 {
            if m_dices[i] == value {
                retValue += 1
            }
        }
        return retValue
    }
    func smallStraight() -> Int {
        var duplic_dices = m_dices
        duplic_dices.sort()
        if duplic_dices.contains(1) {
            if (duplic_dices.contains(2)) && (duplic_dices.contains(3)) && (duplic_dices.contains(4)) {
                return 30
            }
        }
        if duplic_dices.contains(2) {
            if (duplic_dices.contains(3)) && (duplic_dices.contains(4)) && (duplic_dices.contains(5)) {
                return 30
            }
        }
        if duplic_dices.contains(3) {
            if (duplic_dices.contains(4)) && (duplic_dices.contains(5)) && (duplic_dices.contains(6)) {
                return 30
            }
        }
        
        return 0
    }
    func largeStraight() -> Int {
        var duplic_dices = m_dices
        duplic_dices.sort()
        print(duplic_dices)
        if duplic_dices[0] == 1 {
            if (duplic_dices[1] == 2) && (duplic_dices[2] == 3) && (duplic_dices[3] == 4) && (duplic_dices[4] == 5) {
                return 40
            }
        }
        if duplic_dices[0] == 2 {
            if (duplic_dices[1] == 3) && (duplic_dices[2] == 4) && (duplic_dices[3] == 5) && (duplic_dices[4] == 6) {
                return 40
            }
        }

        return 0
    }
    func sumDice() -> Int {
        var sum : Int = 0
        for i in 0..<5 {
            sum += m_dices[i]
        }
        return sum
    }
    func toDoAfterCategory() {
        if allUpperUsed() {
            outletUpperTotal.titleLabel?.text = String(getUpperTotal())
            outletUpperBonus.titleLabel?.text = String(getUpperBonus())
        }
        if allLowerUsed() {
            outletLowerTotal.titleLabel?.text = String(getLowerTotal())
        }
        if allLowerUsed() && allUpperUsed() {
            outletGrandTotal.titleLabel?.text = String(getLowerTotal() + getUpperBonus() + getUpperTotal())
            
            /* Finish Game */
            audioController.playerEffect(name: SoundWin)
            
            let startX : CGFloat = ScreenWidth - 100
            let startY : CGFloat = 0
            let endY : CGFloat = ScreenHeight + 300
            
            let stars = StardustView(frame: CGRect(x: startX, y: startY, width: 10, height: 10))
            self.view.addSubview(stars)
            self.view.sendSubviewToBack(_: stars)
            
            UIView.animate(withDuration: 3.0, delay: 0.0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                stars.center = CGPoint(x: startX, y: endY)
            }, completion: {
                (value:Bool) in stars.removeFromSuperview()
            })
        }
        outletRollDice.isEnabled = true
        
        outletDice1.isEnabled = true
        outletDice1.setTitle("?", for: UIControl.State.normal)
        outletDice1.backgroundColor = UIColor.blue
        if outletDice1.isEnabled {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.outletDice1.center = self.dicePos[0]
                self.outletDice1.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: nil)
        }
        
        outletDice2.isEnabled = true
        outletDice2.setTitle("?", for: UIControl.State.normal)
        outletDice2.backgroundColor = UIColor.blue
        if outletDice2.isEnabled {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.outletDice2.center = self.dicePos[1]
                self.outletDice2.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: nil)
        }
        
        outletDice3.isEnabled = true
        outletDice3.setTitle("?", for: UIControl.State.normal)
        outletDice3.backgroundColor = UIColor.blue
        if outletDice3.isEnabled {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.outletDice3.center = self.dicePos[2]
                self.outletDice3.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: nil)
        }
        
        outletDice4.isEnabled = true
        outletDice4.setTitle("?", for: UIControl.State.normal)
        outletDice4.backgroundColor = UIColor.blue
        if outletDice4.isEnabled {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.outletDice4.center = self.dicePos[3]
                self.outletDice4.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: nil)
        }
        
        outletDice5.isEnabled = true
        outletDice5.setTitle("?", for: UIControl.State.normal)
        outletDice5.backgroundColor = UIColor.blue
        if outletDice5.isEnabled {
            UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
                self.outletDice5.center = self.dicePos[4]
                self.outletDice5.transform = CGAffineTransform(rotationAngle: 0)
            }, completion: nil)
        }
        
        outletRollDice.setTitle("Roll Dice", for: UIControl.State.normal)
        outletGameMessage.text = "게임시작 \"Roll Dice\" 버튼을 누르세요!"
        
        m_roll = 0
        m_round += 1
    }
    func allUpperUsed() -> Bool {
        for i in 0..<7 {
            if m_isUsed[i] == false {
                return false
            }
        }
        return true
    }
    func allLowerUsed() -> Bool {
        for i in 7..<13 {
            if m_isUsed[i] == false {
                return false
            }
        }
        return true
    }
    func getUpperTotal() -> Int {
        var sum : Int = 0
        for i in 0..<7 {
            sum += m_scores[i]
        }
        return sum
    }
    func getUpperBonus() -> Int {
        if getUpperTotal() >= 63 {
            return 35
        }
        return 0
    }
    func getLowerTotal() -> Int {
        var sum : Int = 0
        for i in 7..<13 {
            sum += m_scores[i]
        }
        return sum
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dicePos = [outletDice1.center, outletDice2.center, outletDice3.center, outletDice4.center, outletDice5.center]
        // Do any additional setup after loading the view.
    }


}

