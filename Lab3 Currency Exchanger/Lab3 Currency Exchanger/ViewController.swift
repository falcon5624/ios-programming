//
//  ViewController.swift
//  Lab3 Currency Exchanger
//
//  Created by kpugame on 2020/04/12.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedKeys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.value2, reuseIdentifier: nil)
        let couponPercentage = sortedKeys[indexPath.row]
        let result = possibleCurrency[couponPercentage]!
        cell.textLabel?.text = "\(couponPercentage)% 쿠폰:"
        cell.textLabel?.textColor = UIColor(red: CGFloat(couponPercentage) / 100.0 + 0.3, green: 0, blue: 0, alpha: 1)
        cell.textLabel?.font = UIFont.systemFont(ofSize: 30)
        cell.textLabel?.adjustsFontSizeToFitWidth = true
        cell.detailTextLabel?.text = String(format:"%d $ 환전 가능!", Int(result))
        cell.detailTextLabel?.textColor = UIColor.white
        cell.detailTextLabel?.font = UIFont.systemFont(ofSize: 22)
        cell.backgroundColor = UIColor.black
        return cell
    }
    
    @IBOutlet weak var KRWLabel: UILabel!
    @IBOutlet weak var exchangeRateLabel: UILabel!
    @IBOutlet weak var exchangerRateSlider: UISlider!
    @IBOutlet weak var KRWTextField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    
    let currencyExchanger : CurrencyExchanger = CurrencyExchanger(KRW: 1000.0, ExchangeRate: 1250.0)
    var possibleCurrency = Dictionary<Int, Int>()
    var sortedKeys : [Int] = []
    
    func RefreshUI() {
        KRWTextField.text = String(format: "%d" , Int(currencyExchanger.KRW))
        exchangeRateLabel.text = String(format: "원/달러:%0.2f", currencyExchanger.ExchangeRate)
        exchangerRateSlider.value = Float(currencyExchanger.ExchangeRate)
    }
    
    @IBAction func Calculate(_ sender: UIButton) {
        currencyExchanger.KRW = Float((KRWTextField.text! as NSString).intValue)
        possibleCurrency = currencyExchanger.GetPossibleUSDs()
        sortedKeys = Array(possibleCurrency.keys).sorted()
        tableView.reloadData()
    }
    
    @IBAction func ChangeExchangeRate(_ sender: UISlider) {
        currencyExchanger.ExchangeRate = exchangerRateSlider.value
        RefreshUI()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        RefreshUI()
    }


}

