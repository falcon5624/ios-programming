//
//  Currency Exchanger.swift
//  Lab3 Currency Exchanger
//
//  Created by kpugame on 2020/04/12.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class CurrencyExchanger {
    var KRW : Float                    //  원화
    var ExchangeRate : Float           //  환율, 1000.00 ~ 1300.00
    let ExchangeFee : Float = 0.02    //  환전 수수료
    
    init (KRW : Float, ExchangeRate : Float) {
        self.KRW = KRW
        self.ExchangeRate = ExchangeRate
    }
    
    func Calculate(PreferentialCouponPercentage : Float) -> Int {
        let totalFee : Float = ExchangeFee * (1 - (PreferentialCouponPercentage / 100))
        let USD : Float = KRW / (ExchangeRate * (1 + totalFee))
        return Int(USD)
    }
    
    func GetPossibleUSDs() -> Dictionary<Int, Int> {
        let couponPercentages = [30, 50, 70]
        var returnValue = Dictionary<Int, Int>()
        
        for percentage in couponPercentages {
            returnValue[percentage] = self.Calculate(PreferentialCouponPercentage: Float(percentage))
        }
        return returnValue
    }
}
