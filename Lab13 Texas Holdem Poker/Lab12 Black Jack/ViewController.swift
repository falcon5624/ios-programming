//
//  ViewController.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    /* game variables */
    var player : Player = Player(name : "player")
    var dealer : Player = Player(name : "dealer")
    var community : Player = Player(name : "community")
    
    var audioController : AudioController
    required init?(coder: NSCoder) {
        audioController = AudioController()
        audioController.preloadAudioEffects(audioFileNames: AudioEffectFiles)
        super.init(coder : coder)
    }
    
    var LCardsPlayer = [UIImageView]()
    var LCardsDealer = [UIImageView]()
    var LCardsCommunity = [UIImageView]()
    
    var deck = [Int]()
    var deckIndex = 0
    
    var nBetMoney : Int = 0
    var nPlayerMoney : Int = 1000
    var nState : Int = 0
    
    
    /* outlets */
    @IBOutlet weak var dealerPts: UILabel!
    @IBOutlet weak var playerPts: UILabel!
    @IBOutlet weak var betMoney: UILabel!
    @IBOutlet weak var playerMoney: UILabel!
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var outletCheckButton: UIButton!
    @IBOutlet weak var outletBetX2Button: UIButton!
    @IBOutlet weak var outletBetX1Button: UIButton!
    @IBOutlet weak var outletDealButton: UIButton!
    @IBOutlet weak var outletAgainButton: UIButton!
    
    
    /* action functions */
    @IBAction func check(_ sender: Any) {
        /* */
        playerMoney.text = "You have $\(nPlayerMoney)"
        playerMoney.sizeToFit()
        if nState == 4 {
            checkWinner()
            nState = 0
            outletCheckButton.isEnabled = false
            outletBetX1Button.isEnabled = false
            outletBetX2Button.isEnabled = false
            
            outletDealButton.isEnabled = false
            outletAgainButton.isEnabled = true
        } else {
            outletCheckButton.isEnabled = false
            outletBetX2Button.isEnabled = false
            outletBetX1Button.isEnabled = false
            
            outletDealButton.isEnabled = true
            outletAgainButton.isEnabled = false
        }
    }
    @IBAction func betx2(_ sender: Any) {
        var oldBetMoney = nBetMoney
        nBetMoney *= 3
        if nBetMoney <= nPlayerMoney {
            betMoney.text = "$\(nBetMoney)"
            nPlayerMoney -= (nBetMoney - oldBetMoney)
            playerMoney.text = "You have $\(nPlayerMoney)"
            playerMoney.sizeToFit()
            
            outletDealButton.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        } else {
            nBetMoney /= 3
        }
        
        if nState == 4 {
            checkWinner()
            nState = 0
            outletCheckButton.isEnabled = false
            outletBetX1Button.isEnabled = false
            outletBetX2Button.isEnabled = false
            
            outletDealButton.isEnabled = false
            outletAgainButton.isEnabled = true
        } else {
            outletCheckButton.isEnabled = false
            outletBetX2Button.isEnabled = false
            outletBetX1Button.isEnabled = false
            
            outletDealButton.isEnabled = true
            outletAgainButton.isEnabled = false
        }
    }
    @IBAction func betx1(_ sender: Any) {
        nBetMoney *= 2
        if nBetMoney <= nPlayerMoney {
            betMoney.text = "$\(nBetMoney)"
            nPlayerMoney -= (nBetMoney / 2)
            playerMoney.text = "You have $\(nPlayerMoney)"
            playerMoney.sizeToFit()
            
            outletDealButton.isEnabled = true
            audioController.playerEffect(name: SoundChip)
        } else {
            nBetMoney /= 2
        }
        
        if nState == 4 {
            checkWinner()
            nState = 0
            outletCheckButton.isEnabled = false
            outletBetX1Button.isEnabled = false
            outletBetX2Button.isEnabled = false
            
            outletDealButton.isEnabled = false
            outletAgainButton.isEnabled = true
        } else {
            outletCheckButton.isEnabled = false
            outletBetX2Button.isEnabled = false
            outletBetX1Button.isEnabled = false
            
            outletDealButton.isEnabled = true
            outletAgainButton.isEnabled = false
        }
    }
    @IBAction func deal(_ sender: Any) {
        self.deal()
    }
    @IBAction func again(_ sender: Any) {
        player.reset()
        dealer.reset()
        community.reset()
        
        nBetMoney = 10
        betMoney.text = "$" + String(nBetMoney)
        for card in LCardsDealer {
            card.removeFromSuperview()
        }
        for card in LCardsPlayer {
            card.removeFromSuperview()
        }
        for card in LCardsCommunity {
            card.removeFromSuperview()
        }
        
        LCardsDealer = [UIImageView]()
        LCardsPlayer = [UIImageView]()
        LCardsCommunity = [UIImageView]()
        
        dealerPts.text = "Dealer: "
        status.text = "Please Bet!"
        playerPts.text = "Player: "
        
        self.outletDealButton.isEnabled = true
        self.outletBetX1Button.isEnabled = true
        self.outletBetX2Button.isEnabled = true
        self.outletCheckButton.isEnabled = true
    }
    
    func checkPlayer(input : Player) -> Int {
        var cardlist : [Card] = community.cards
        var countlist : [Int] = []
        var cardnumlist : [Int] = []
        for card in input.cards {
            cardlist.append(card)
        }
        for _ in 1...13 {
            countlist.append(0)
        }
        
        cardlist = cardlist.sorted(by: {$0.getValue() > $1.getValue()})
        
        var flushNum : Int = 0
        for i in 0..<6 {
            flushNum = 0
            for j in i+1..<7 {
                if cardlist[i].getX() == cardlist[j].getX() {
                    flushNum += 1
                }
            }
            if flushNum >= 4 {
                break
            }
        }
        
        for card in cardlist {
            print("\(input.getName())  \(card.getSuit())::\(card.getValue())")
            countlist[card.getValue() - 1] += 1
            cardnumlist.append(card.getValue())
        }
        
        var pair = 0
        var triple = 0
        var fourCard = 0
        
        for idx in 0...12 {
            if countlist[idx] == 2 {
                pair += 1
            }
            if countlist[idx] == 3 {
                triple += 1
            }
            if countlist[idx] == 4 {
                fourCard += 1
            }
        }
        
        var isStraight : Bool = false
        if cardnumlist.contains(1) {
            if cardnumlist.contains(2) && cardnumlist.contains(3) && cardnumlist.contains(4) && cardnumlist.contains(5) {
                isStraight = true
            }
            
        } else if cardnumlist.contains(2) {
            if cardnumlist.contains(3) && cardnumlist.contains(4) && cardnumlist.contains(5) && cardnumlist.contains(6) {
                isStraight = true
            }
        } else if cardnumlist.contains(3) {
            if cardnumlist.contains(4) && cardnumlist.contains(5) && cardnumlist.contains(6) && cardnumlist.contains(7) {
                isStraight = true
            }
        } else if cardnumlist.contains(4) {
            if cardnumlist.contains(5) && cardnumlist.contains(6) && cardnumlist.contains(7) && cardnumlist.contains(8) {
                isStraight = true
            }
        } else if cardnumlist.contains(5) {
            if cardnumlist.contains(6) && cardnumlist.contains(7) && cardnumlist.contains(8) && cardnumlist.contains(9) {
                isStraight = true
            }
        } else if cardnumlist.contains(6) {
            if cardnumlist.contains(7) && cardnumlist.contains(8) && cardnumlist.contains(9) && cardnumlist.contains(10) {
                isStraight = true
            }
        } else if cardnumlist.contains(7) {
            if cardnumlist.contains(8) && cardnumlist.contains(9) && cardnumlist.contains(10) && cardnumlist.contains(11) {
                isStraight = true
            }
        } else if cardnumlist.contains(8) {
            if cardnumlist.contains(9) && cardnumlist.contains(10) && cardnumlist.contains(11) && cardnumlist.contains(12) {
                isStraight = true
            }
        } else if cardnumlist.contains(9) {
            if cardnumlist.contains(10) && cardnumlist.contains(11) && cardnumlist.contains(12) && cardnumlist.contains(13) {
                isStraight = true
            }
        }
        
        if fourCard != 0 {
            /* four cards */
            return 90
        }
        if cardnumlist.contains(1) && cardnumlist.contains(2) && cardnumlist.contains(3) && cardnumlist.contains(4) && cardnumlist.contains(5) {
          
            /* back straight */
            return 50
        }
        if cardnumlist.contains(1) && cardnumlist.contains(10) && cardnumlist.contains(11) && cardnumlist.contains(12) && cardnumlist.contains(13) {
           
            /* mountain */
            return 60
        }
        if  isStraight {
            
            /* straight */
            return 40
        }
        if flushNum >= 4 {
            /* flush */
            return 70
        }
        if pair > 0 {
            if (triple != 0) && (pair == 1) {
                /* fullhouse */
                return 80
            }
            if flushNum >= 4 {
                /* flush */
                return 70
            }
            if pair == 1 {
                /* one pair */
                return 10
            }
            if pair == 2 {
                /* two pairs */
                return 20
            }
            
        }
        if triple != 0 {
            /* triple */
            return 30
        }
        
        /* none */
        return 0
    }
    
    func setPointText(point : Int) -> String {
        var text = ""
        switch point {
        case 10:
            text = "One Pair"
            break
        case 20:
            text = "Two Pair"
            break
        case 30:
            text = "Triple"
            break
        case 40:
            text = "Straight"
            break
        case 50:
            text = "Back Straight"
            break
        case 60:
            text = "Mountain"
            break
        case 70:
            text = "Flush"
            break
        case 80:
            text = "Full House"
            break
        case 90:
            text = "Four Cards"
            break
        default:
            text = "None"
        }
        return text
    }

    func checkWinner() {
        /* 딜러 카드 공개 */
        for index in 0...1 {
            LCardsDealer[index].removeFromSuperview()
            let newImageView = UIImageView(image: UIImage(named: dealer.cards[index].filename())!)
            newImageView.center = CGPoint(x: 200 + index * 75, y: 250)
            
            self.view.addSubview(newImageView)
            LCardsDealer[index] = newImageView
        }
        
        /* 여기를 작성하자 */
        var playerPoint = 0
        var dealerPoint = 0
        playerPoint = checkPlayer(input: player)
        dealerPoint = checkPlayer(input: dealer)
        
        
        // 플레이어와 딜러 점수 판정 후 출력
        var playerText = ""
        var dealerText = ""
        playerText = setPointText(point: playerPoint)
        dealerText = setPointText(point: dealerPoint)
        
        playerPts.text = "Player: " + playerText
        dealerPts.text = "Dealer: " + dealerText
        
        if playerPoint > dealerPoint {
            status.text = "Player Win"
            nPlayerMoney += nBetMoney * 2
            audioController.playerEffect(name: SoundWin)
        } else {
            status.text = "Player Lose"
            audioController.playerEffect(name: SoundWrong)
        }
        /*
        if player.value() > 21 {
            status.text = "Player Busts"
            audioController.playerEffect(name: SoundWrong)
        } else if dealer.value() > 21 {
            status.text = "Dealer Busts"
            nPlayerMoney += nBetMoney * 2
            audioController.playerEffect(name: SoundWin)
        } else if dealer.value() == player.value() {
            status.text = "Push"
            nPlayerMoney += nBetMoney
            audioController.playerEffect(name: SoundDing)
        } else if dealer.value() < player.value() {
            status.text = "You won!"
            nPlayerMoney += nBetMoney * 2
            audioController.playerEffect(name: SoundWin)
        } else {
            status.text = "Sorry You Lost"
            audioController.playerEffect(name: SoundWrong)
        }
 */
        nBetMoney = 0
        playerMoney.text = "You have $\(nPlayerMoney)"
        playerMoney.sizeToFit()
        betMoney.text = "$" + String(nBetMoney)
    }
    
    func deal() {
        if nState < 4 {
            nState += 1
        }
        else {
            // 오류 방지
            self.outletDealButton.isEnabled = false
            return
        }
        switch self.nState {
        case 1:
            // 덱을 섞고
            deck.removeAll()
            for i in 0...51 {
                deck.append(i)
            }
            cheatPlayer(n: 0)
            cheatPlayer(n: 1)
            deck.shuffle()
            deckIndex = 0
            
            // 카드 두장 받아야함
            for i in 0...1 {
                //drawPlayer(n: i)
                drawDealer(n: i)
            }
            
            break
        case 2:
            // 커뮤니티 카드 3장
            for i in 0...2 {
                drawCommunity(n: i)
                //cheatStriaght(n: i)
                //cheatFlush(n: i)
            }
            break
        case 3:
            // 커뮤니티 카드 1장 추가
            drawCommunity(n: 3)
            //cheatStriaght(n: 3)
            //cheatFlush(n: 3)
            break
        case 4:
            // 커뮤니티 카드 마지막 1장 추가
            drawCommunity(n: 4)
            //cheatStriaght(n: 4)
            //cheatFlush(n: 4)
            break
        default:
            break
        }
        
        self.outletDealButton.isEnabled = false
        self.outletBetX1Button.isEnabled = true
        self.outletBetX2Button.isEnabled = true
        self.outletCheckButton.isEnabled = true
    }
    func drawCommunity(n : Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        community.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 335 + n * 75, y: 350)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsCommunity.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func drawDealer(n: Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        dealer.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: "b1fv")!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 200 + n * 75, y: 250)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsDealer.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func drawPlayer(n: Int) {
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        player.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 200 + n * 75, y: 450)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsPlayer.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func cheatPlayer (n: Int) {
        let newCard = Card(temp:deck[n * 13 - n])
        deck.remove(at:n * 13 + 1)
        
        player.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 200 + n * 75, y: 450)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsPlayer.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func cheatFlush(n : Int) {
        deck.sort()
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 2
        
        community.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 335 + n * 75, y: 350)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsCommunity.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    func cheatStriaght(n: Int) {
        deck.sort()
        let newCard = Card(temp:deck[deckIndex])
        deckIndex += 1
        
        community.addCard(c:newCard)
        let newImageView = UIImageView(image: UIImage(named: newCard.filename())!)
        newImageView.center = CGPoint(x: 1000, y: 150)
        
        self.view.addSubview(newImageView)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, options: UIView.AnimationOptions.curveEaseInOut, animations: {
            newImageView.center = CGPoint(x: 335 + n * 75, y: 350)
            newImageView.transform = CGAffineTransform(rotationAngle: 3.14)
        }, completion: nil)
        
        LCardsCommunity.append(newImageView)
        audioController.playerEffect(name: SoundFlip)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        for i in 0...51 {
            deck.append(i)
        }
        
        nBetMoney = 10
        betMoney.text = "$\(nBetMoney)"
        playerMoney.text = "You have $\(nPlayerMoney)"
        dealerPts.text = "Dealer: "
        status.text = "Please Bet!"
        playerPts.text = "Player: "
    }


}

