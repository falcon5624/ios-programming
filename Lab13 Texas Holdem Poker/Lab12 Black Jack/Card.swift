//
//  Card.swift
//  Lab12 Black Jack
//
//  Created by kpugame on 2020/06/09.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import Foundation

class Card {
    private var value : Int
    private var x : Int
    private var suit : String
    init(temp : Int) {
        self.value = temp % 13 + 1
        self.x = temp / 13
        self.suit = ""
    }
    public func getValue() -> Int {
        return self.value
    }
    public func getSuit() -> String {
        switch x {
        case 0:
            self.suit = "Clubs"
            break
        case 1:
            self.suit = "Hearts"
            break
        case 2:
            self.suit = "Diamonds"
            break
        case 3:
            self.suit = "Spades"
            break
        default:
            break
        }
        return self.suit
    }
    public func getX() -> Int {
        return self.x
    }
    func filename() -> String {
        return getSuit() + String(self.value)
    }
}
