//
//  ViewController.swift
//  Lab1 Stop Watch
//
//  Created by kpugame on 2020/03/27.
//  Copyright © 2020 totobutt5624. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    /* 스탑워치 시간 변수 */
    var minfront : UInt = 0     //  10의 자리 분
    var minback : UInt = 0      //  1의 자리 분
    var secfront : UInt = 0     //  10의 자리 초
    var secback : UInt = 0      //  1의 자리 초
    var desisec : UInt = 0      //  1/10 초
    
    /* 타이머 인스턴스 */
    var timer = Timer()
    
    /* 라벨과 버튼 */
    @IBOutlet var timeLabel : UILabel!
    @IBOutlet var StartButton : UIButton!

    /* 스탑워치 메서드 시작 부분 */
    @IBAction func Stop() {
        /* 스톱워치를 멈춥니다.
         재생 상태여야 합니다.
         */
        if (timer.isValid) {
            StartButton.titleLabel!.text = "ReSRT"
        }
        timer.invalidate()
    }
    
    @IBAction func Reset() {
        /* 스톱워치를 초기화합니다.
         시간 라벨의 시간 변수들이 모두 0으로 초기화됩니다.
         */
        timer.invalidate()
        Setup()
        StartButton.titleLabel?.text = "START"
    }
    
    func Setup() {
        /* 스톱워치 기능과 멤버들을 초기화합니다. */
        self.minfront = 0
        self.minback = 0
        self.secfront = 0
        self.secback = 0
        self.desisec = 0
        timeLabel.text = "\(minfront)\(minback):\(secfront)\(secback)::\(desisec)"
    }
    
    @IBAction func Start () {
        /*  스톱워치를 시작합니다
         1/10초 마다 타이머 이벤트를 발생시킵니다.
         타이머가 실행 중이라면 실행되지 않습니다.
         */
        if (!timer.isValid) {
            timer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(self.Update), userInfo: nil, repeats: true)
        }
    }
    
    @objc func Update() {
        desisec += 1
        if (desisec == 10) {
            desisec = 0
            secback += 1
        }
        if (secback == 10) {
            secback = 0
            secfront += 1
        }
        if (secfront == 6) {
            secfront = 0
            minback += 1
        }
        if (minback == 10) {
            minback = 0
            minfront += 1
        }
        timeLabel.text = "\(minfront)\(minback):\(secfront)\(secback)::\(desisec)"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        Setup()
    }
}

