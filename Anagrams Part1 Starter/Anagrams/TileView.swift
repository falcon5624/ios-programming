//
//  TileView.swift
//  Anagrams
//
//  Created by kpugame on 2020/06/04.
//  Copyright © 2020 Caroline. All rights reserved.
//

import UIKit

class TileView : UIImageView {
    var letter : Character
    var isMatched : Bool = false
    private var xOffset : CGFloat = 0.0
    private var yOffset : CGFloat = 0.0
    
    required init?(coder: NSCoder) {
        fatalError("use init(letter:, sideLength:)")
    }
    
    init(letter : Character, sideLength : CGFloat) {
        self.letter = letter
        let image = UIImage(named: "tile")!
        
        super.init(image : image)
        
        let scale = sideLength / image.size.width
        self.frame = CGRect(x: 0, y: 0, width: image.size.width * scale, height: image.size.height * scale)
        
        let letterLabel = UILabel(frame: self.bounds)
        letterLabel.textAlignment = NSTextAlignment.center
        letterLabel.textColor = UIColor.white
        letterLabel.backgroundColor = UIColor.clear
        letterLabel.text = String(letter).uppercased()
        letterLabel.font = UIFont(name: "Verdana-Bold", size: 78 * scale)
        self.addSubview(letterLabel)
        self.isUserInteractionEnabled = true
    }
    
    func randomize() {
        let rotation = CGFloat(randomNumber(minX: 0, maxX: 50)) / 100 - 0.2
        
        self.transform =  CGAffineTransform(rotationAngle: rotation)
        
        let yOffset = CGFloat(randomNumber(minX: 0, maxX: 10) - 10)
        self.center = CGPoint(x: self.center.x, y: self.center.y + yOffset)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let touchPoint = touch.location(in: self.superview)
            xOffset = touchPoint.x - self.center.x
            yOffset = touchPoint.y - self.center.y
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
}
